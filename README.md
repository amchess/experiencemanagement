# Introduction

ExperienceManagement is a JavaFX chess application to

manage an experience file.

## To use with maven

For example, in eclipse, create three configurations (Right click, on the project folder, Run as ->Maven build ...)

The JavaFX-Maven-Plugin is usable for development (via mvn jfx:run) and deployment (via mvn jfx:jar or mvn jfx:native).
http://pgkaila.github.io/javafx-maven-plugin/basic-archetype.html
-Name: JFX JAR Goals: jfx:jar //create a jar of the application
-Name: JFX NATIVE Goals: jfx:native //To distribute your JFX application with a native installer
-Name: JFX RUN Goals: jfx:run //Start the JavaFX application

## To install ictk (Internet Chess ToolKit) artifact:

mvn install:install-file -Dfile=.\ictk-1.1.0.jar -DpomFile=.\pom.xml