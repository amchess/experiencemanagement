package com.alphachess.util;

import static com.alphachess.util.Constants.TEST_FEN;
import static com.alphachess.util.GenericUtil.getAlgebricMove;
import static com.alphachess.util.GenericUtil.getAlgebricSquare;
import static com.alphachess.util.GenericUtil.getFromSquare;
import static com.alphachess.util.GenericUtil.getIntSquare;
import static com.alphachess.util.GenericUtil.getMoveInt;
import static com.alphachess.util.GenericUtil.getToSquare;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import ictk.boardgame.chess.ChessBoard;
import ictk.boardgame.chess.ChessMove;
import ictk.boardgame.chess.io.FEN;

public class GenericUtilTest {

	@Test
	void getFile() {
		File file=new File("pippo.ext");
		File file1=GenericUtil.getFile(file,1);
		assertTrue(file1.getName().equals("pippo1.ext")&&file1.length()==0);
		file.delete();
		file1.delete();
	}

	@Test
	void intMove() {
		int fromSquare = getFromSquare(731);
		assertTrue(getAlgebricSquare(fromSquare).equals("d2"));
		int toSquare = getToSquare(731);
		assertTrue(getAlgebricSquare(toSquare).equals("d4"));
		String algebricMove = getAlgebricMove(731);
		assertTrue(algebricMove.equals("d2d4"));
	}
	

	@Test
	void getMoveIntFromChessMove() {
		try {
			ChessBoard chessBoard=(ChessBoard)((new FEN()).stringToBoard(TEST_FEN));
			ChessMove chessMove=(ChessMove)GenericUtil.getLegalMoves(chessBoard).get(0);
			int moveInt=getMoveInt(chessMove);
			assertTrue(moveInt==3112);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void getIntSquareFromAlgebric() {
		int intSquare=getIntSquare("a8");
		assertTrue(intSquare==56);
	}

}
