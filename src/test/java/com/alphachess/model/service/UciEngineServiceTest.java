package com.alphachess.model.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.alphachess.model.dao.UciEngineDao;
import com.alphachess.model.service.impl.UciEngineServiceImpl;

import javafx.collections.ObservableMap;

public class UciEngineServiceTest {
	private UciEngineService uciEngineService;
	private UciEngineDao uciEngineDaoMock;
	@BeforeEach
	public void init() {
		if (Objects.isNull(this.uciEngineService)) {
			this.uciEngineService = new UciEngineServiceImpl();
		}
		if (Objects.isNull(this.uciEngineDaoMock)) {
			this.uciEngineDaoMock = mock(UciEngineDao.class);
			this.uciEngineService.setUciEngineDao(this.uciEngineDaoMock);
		}
	}
	@Test
	public void getHashKeyFenHashMap() {
		when(this.uciEngineDaoMock.getFensListInHashKeyFenHashMap(anyListOf(String.class), any(String.class), any(File.class))).thenReturn(new HashMap<String,String>());
		ObservableMap<String, String> fxMap=this.uciEngineService.getFensListInHashKeyFenHashMap(Collections.emptyList(),"",null);
		assertTrue(fxMap.isEmpty());
	}
}
