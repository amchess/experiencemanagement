package com.alphachess.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.alphachess.model.dao.LearningFileEntryDao;
import com.alphachess.model.domain.LearningFileEntry;
import com.alphachess.model.fx.FXLearningFileEntry;
import com.alphachess.model.service.impl.LearningFileEntryServiceImpl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class LearningFileEntryServiceTest {

	private static final List<LearningFileEntry> entries = new ArrayList<LearningFileEntry>(Arrays
			.<LearningFileEntry>asList(new LearningFileEntry(new BigInteger("16246198974017150361"), 29, -1, 861, 0),
					new LearningFileEntry(new BigInteger("7868765710304557961"), 29, -244, 2592, 0),
					new LearningFileEntry(new BigInteger("8988295494880255566"), 27, -582, 3323, 0)));

	private static final ObservableList<FXLearningFileEntry> fxLearningFileEntries = FXCollections.observableArrayList(
			new FXLearningFileEntry("16246198974017150361", 29, -1, 861, 0),
			new FXLearningFileEntry("7868765710304557961", 29, -244, 2592, 0),
			new FXLearningFileEntry("8988295494880255566", 27, -582, 3323, 0));


	private static final LearningFileEntry learningFileEntryTest = new LearningFileEntry(
			new BigInteger("8988295494880255236"), 21, -582, 3323, 0);

	private static final FXLearningFileEntry fxlearningFileEntryTest = new FXLearningFileEntry("8988295494880255236",
			21, -582, 3323, 0);

	private LearningFileEntryService learningFileEntryService;
	private LearningFileEntryDao learningFileEntryDaoMock;

	@BeforeEach
	public void init() {
		if (Objects.isNull(this.learningFileEntryService)) {
			this.learningFileEntryService = new LearningFileEntryServiceImpl();
		}
		if (Objects.isNull(this.learningFileEntryDaoMock)) {
			this.learningFileEntryDaoMock = mock(LearningFileEntryDao.class);
			this.learningFileEntryService.setLearningFileEntryDao(this.learningFileEntryDaoMock);
		}

	}

	@Test
	public void getAll() {
		when(this.learningFileEntryDaoMock.getAll("")).thenReturn(entries);
		ObservableList<FXLearningFileEntry> list = this.learningFileEntryService.getAll("");
		assertTrue(list.size() == 3);
		assertEquals(fxLearningFileEntries, list);
	}

    @Test
	public void createOrUpdate() {
		this.learningFileEntryService.createOrUpdate(fxLearningFileEntries, fxlearningFileEntryTest);
		verify(this.learningFileEntryDaoMock).createOrUpdate(entries, learningFileEntryTest);
	}

}
