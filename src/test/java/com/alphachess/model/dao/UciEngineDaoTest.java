package com.alphachess.model.dao;
import static com.alphachess.util.Constants.INITIAL_FEN;
import static com.alphachess.util.Constants.INITIAL_FEN_HASHKEY;
import static com.alphachess.util.Constants.TEST_FEN;
import static com.alphachess.util.Constants.TEST_FEN_HASHKEY;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.alphachess.model.dao.impl.UciEngineDaoImpl;
public class UciEngineDaoTest {
	UciEngineDao uciEngineDao = new UciEngineDaoImpl();

	@Test
	void getHashKeyFenHashMap() {
		File hashKeyGeneratorFile = new File("src/test/resources/hashkeygenerator.exe");		
		List<String> inputFens=new ArrayList<>();
		inputFens.add(INITIAL_FEN);
		inputFens.add(TEST_FEN);
		Map<String,String> resultHashMap=uciEngineDao.getFensListInHashKeyFenHashMap(inputFens,hashKeyGeneratorFile.getAbsolutePath(),null);
		assertTrue(resultHashMap.get(INITIAL_FEN_HASHKEY.toString()).equals(INITIAL_FEN)&&resultHashMap.get(TEST_FEN_HASHKEY.toString()).equals(TEST_FEN));
	}
}
