package com.alphachess.model.dao;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.alphachess.model.dao.impl.LearningFileEntryDaoImpl;
import com.alphachess.model.dao.impl.UciEngineDaoImpl;
import com.alphachess.model.domain.LearningFileEntry;

public class LearningFileEntryDaoTest {
	LearningFileEntryDao learningFileEntryDao = new LearningFileEntryDaoImpl();
	UciEngineDao uciEngineDao = new UciEngineDaoImpl();

	@Test
	void getByHashKeyExisting() {
		List<LearningFileEntry> learningFileEntries = getLearningFileEntries();
		LearningFileEntry learningFileEntryResult = learningFileEntryDao.getByHashKey(learningFileEntries,
				new BigInteger("5398946861044308550"));
		assertNotNull(learningFileEntryResult);
	}

	@Test
	void getByHashKeyNotExisting() {
		List<LearningFileEntry> learningFileEntries = getLearningFileEntries();
		LearningFileEntry learningFileEntryResult = learningFileEntryDao.getByHashKey(learningFileEntries,
				new BigInteger("1111"));
		assertNull(learningFileEntryResult);
	}

	@Test
	void create() {
		List<LearningFileEntry> learningFileEntries = getLearningFileEntries();
		LearningFileEntry learningFileEntryToCreate = getLearningFileEntry(10, new BigInteger("8143574663265718438"),
				732, (byte) 53, 54);
		learningFileEntryDao.createOrUpdate(learningFileEntries, learningFileEntryToCreate);
		assertTrue(learningFileEntries.size() == 3);
	}

	@Test
	void update() {
		List<LearningFileEntry> learningFileEntries = getLearningFileEntries();
		LearningFileEntry learningFileEntryToUpdate = getLearningFileEntry(12, new BigInteger("2676432946319827165"),
				999, (byte) 51, 50);
		learningFileEntryDao.createOrUpdate(learningFileEntries, learningFileEntryToUpdate);
		assertTrue(learningFileEntries.get(0).getMove() == 999);
	}

	@Test
	void deleteExisting() {
		List<LearningFileEntry> learningFileEntries = getLearningFileEntries();
		learningFileEntryDao.delete(learningFileEntries, new BigInteger("2676432946319827165"));
		assertTrue(learningFileEntries.size() == 1);
	}

	@Test
	void deleteNotExisting() {
		List<LearningFileEntry> learningFileEntries = getLearningFileEntries();
		learningFileEntryDao.delete(learningFileEntries, new BigInteger("1111"));
		assertTrue(learningFileEntries.size() == 2);
	}

	@Test
	void insertAll() {
		File experienceBinFile = new File("src/test/resources/experience.exp");
		try {
			byte[] experienceBinFileBytes = Files.readAllBytes(experienceBinFile.toPath());
			List<LearningFileEntry> learningFileEntries = learningFileEntryDao
					.getAll(experienceBinFile.getAbsolutePath());
			learningFileEntryDao.insertAll("src/test/resources/temp.exp", learningFileEntries);
			File tempFile = new File("src/test/resources/temp.exp");
			byte[] tempFileBytes = Files.readAllBytes(tempFile.toPath());
			assertTrue(Arrays.equals(experienceBinFileBytes, tempFileBytes));
			tempFile.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	void getAll() {
		File experienceBinFile = new File("src/test/resources/experience.exp");
		List<LearningFileEntry> learningFileEntries = learningFileEntryDao.getAll(experienceBinFile.getAbsolutePath());
		assertTrue(learningFileEntries.size() == 327);
	}

	private List<LearningFileEntry> getLearningFileEntries() {
		List<LearningFileEntry> learningFileEntries = new ArrayList<>();
		learningFileEntries.add(getLearningFileEntry(12, new BigInteger("2676432946319827165"), 731, (byte) 0, 50));
		learningFileEntries.add(getLearningFileEntry(10, new BigInteger("5398946861044308550"), 730, (byte) 0, 51));
		return learningFileEntries;
	}

	private LearningFileEntry getLearningFileEntry(int depth, BigInteger hashKey, int move, byte performance,
			int score) {
		LearningFileEntry learningFileEntry = new LearningFileEntry();
		learningFileEntry.setDepth(10);
		learningFileEntry.setHashKey(hashKey);
		learningFileEntry.setMove(move);
		learningFileEntry.setPerformance(performance);
		learningFileEntry.setScore(score);
		return learningFileEntry;
	}
}
