package com.alphachess.batch;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alphachess.util.FenResult;

import ictk.boardgame.AmbiguousMoveException;
import ictk.boardgame.History;
import ictk.boardgame.IllegalMoveException;
import ictk.boardgame.chess.ChessBoard;
import ictk.boardgame.chess.ChessGame;
import ictk.boardgame.chess.ChessGameInfo;
import ictk.boardgame.chess.ChessMove;
import ictk.boardgame.chess.ChessResult;
import ictk.boardgame.chess.io.FEN;
import ictk.boardgame.chess.io.PGNReader;
import ictk.boardgame.chess.io.PGNWriter;
import ictk.boardgame.io.InvalidGameFormatException;

public class DatabaseImprover {
	public static final int DRAW_INT = 2;
	public static final int BLACK_WIN_INT = 1;
	public static final int WHITE_WIN_INT = 0;	
	public static final int THREAD_SLEEP_VALUE = 500;
	public static final int DRAW_THREESHOLD = 25;
	PrintWriter pw = null;
	private Properties databaseImproverProperties;
	PGNWriter pgnWriter = null;
	private String uciEngineFullPath;
	private int threadsNumber;
	private int hashSizeMB;
	private int cpuMhz;
	private String sygyzyPath;
	private String sygyzyProbeDepth;
	private String dbFullPath;
	private String dbFullPathImproved;
	private int leveledElo;
	private InputStream engineIn;
	private BufferedOutputStream engineOut;
	private Thread engineReadThread;
	private boolean currentFenAnalysisRunning = false;
	long numeroFenTotali;
	long numeroFenInserite;
	boolean currentGameInserted;
	Date dataInizio;
	private long strongestAverageTimeSecondsForMove;
	private List<FenResult> latestFensResults=new ArrayList<>();
	private PGNReader pgnReader;
	private FileReader gamesFr;
	private BufferedReader gamesInputBr;
	private List<ChessGame> inputGames=new ArrayList<>();
	long numeroPartiteTotali = 0;
	long numeroPartiteInserite = 0;
	long numeroPartiteInFormatoScorretto = 0;
	long partitaCorrente = 0;
	FEN fen = new FEN();
	
	public static void main(String[] args) {
		DatabaseImprover databaseImprover = new DatabaseImprover(args);
		databaseImprover.improve();
	}
	public DatabaseImprover(String[] args) {
		setDatabaseImproverProperties(getDatabaseImproverProperties(args));
		setInputParameters(databaseImproverProperties);
	}
	private void setInputParameters(Properties databaseImproverProperties) {
		setUciEngineFullPath(databaseImproverProperties.getProperty("engineFullPath"));
		setThreadsNumber(Integer.parseInt(databaseImproverProperties.getProperty("threadsNumber")));
		setCpuMhz(Integer.parseInt(databaseImproverProperties.getProperty("cpuMhz")));
		setHashSizeMB(Integer.parseInt(databaseImproverProperties.getProperty("hashSizeMB")));
		setSygyzyPath(databaseImproverProperties.getProperty("sygyzyPath"));
		setSygyzyProbeDepth(databaseImproverProperties.getProperty("sygyzyProbeDepth"));
		setDbFullPath(databaseImproverProperties.getProperty("dbFullPath"));
		setDbFullPathImproved(databaseImproverProperties.getProperty("dbFullPathImproved"));
		setLeveledElo(Integer.parseInt(databaseImproverProperties.getProperty("leveledElo")));
		setStrongestAverageTimeSecondsForMove(getStrongestAverageTimeSecondsForMoveFromPCConfiguration());
		try {
			pw = new PrintWriter(new FileWriter(dbFullPathImproved,
					true));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pgnWriter = new PGNWriter(pw);
	}
	private void closeOutput() {
		pw.close();
		pgnWriter.close();
	}
	public Properties getDatabaseImproverProperties() {
		return databaseImproverProperties;
	}
	public void setDatabaseImproverProperties(Properties databaseImproverProperties) {
		this.databaseImproverProperties = databaseImproverProperties;
	}
	private void scriviPgnOutput(ChessGame game) {
		try {
			pgnWriter.writeGame(game);
			pw.print("\r\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public long getStrongestAverageTimeSecondsForMoveFromPCConfiguration() {
		return (long)(getHashSizeMB()*512/(getThreadsNumber()*getCpuMhz()));
	}
	
	private void improve() {
		System.out.println("Begin games cut based on eco code");
		dataInizio = new Date();
		setLatestFensFromPgnAndTruncateGames();
		endFirstElaboration(dataInizio);
		setLatestFensResult();
		writePgnOutput();
		closeOutput();
		System.out.println("End games cut based on eco code");
	}
	private void writePgnOutput() {
		int currentGameNumber=0;
		for(ChessGame currentChessGame:inputGames) {
			int currentResultInt=latestFensResults.get(currentGameNumber).getResult();
			ChessGameInfo chessGameInfo=(ChessGameInfo)currentChessGame.getGameInfo();
			chessGameInfo.setWhiteRating(leveledElo);
			chessGameInfo.setBlackRating(leveledElo);
			ChessResult result=(ChessResult)chessGameInfo.getResult();
			if(currentResultInt==WHITE_WIN_INT) {
				result.set(ChessResult.WHITE_WIN);
			}
			if(currentResultInt==BLACK_WIN_INT) {
				result.set(ChessResult.BLACK_WIN);
			}
			if(currentResultInt==DRAW_INT) {
				result.set(ChessResult.DRAW);
			}
			currentChessGame.setResult(result);
			scriviPgnOutput(currentChessGame);
			currentGameNumber++;
		}
	}
	public String getSygyzyProbeDepth() {
		return sygyzyProbeDepth;
	}
	public void setSygyzyProbeDepth(String sygyzyProbeDepth) {
		this.sygyzyProbeDepth = sygyzyProbeDepth;
	}
	private void setLatestFensResult() {
		dataInizio = new Date();
		System.out.println("Begin correct results computation");
		numeroFenTotali = 0;
		startWithEngineCommunication();
		for(FenResult currentFenResult:latestFensResults){
			String currentFen = currentFenResult.getFen();
			if (!currentFen.equals("")) {
				setOptionsCommand();
				process(currentFenResult);
				setCurrentFenAnalysisRunning(false);
			}
		}
		issueCommand("quit\n");
		getEngineReadThread().interrupt();
		endResultInsert(dataInizio);
	}
	private void endResultInsert(Date dataInizio) {
		System.out.println("Total fens " + numeroFenTotali);
		System.out.println("Inserted fens " + numeroFenInserite);
		Date dataFine = new Date();
		long tempoImpiegato = dataFine.getTime() - dataInizio.getTime();
		long minutiImpiegati = tempoImpiegato / 60000;
		double secondiImpiegati = ((float) tempoImpiegato % 60000) / 1000;
		System.out.println("End process. " + "\r\n"
				+ "Used minutes : " + minutiImpiegati + "\r\n"
				+ "Used seconds : " + secondiImpiegati);
	}	
	private void setOptionsCommand() {
		String optionsCommand = "setoption name Hash value " + hashSizeMB
				+ "\nsetoption name Threads value " + threadsNumber + "\n"
				+ "setoption name SygyzyPath value " + sygyzyPath + "\n"
				+ "setoption name SygyzyProbeDepth value " + sygyzyProbeDepth + "\n";
		issueCommand(optionsCommand);
	}

	private void process(FenResult fenResult) {
		setCurrentFenAnalysisRunning(true);
		startReadThread(fenResult);
		issueGoInfiniteCommand(fenResult.getFen());
		try {
			Thread.sleep(strongestAverageTimeSecondsForMove * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			Thread.currentThread().interrupt();
		}
		issueCommand("stop\n");
		while (currentFenAnalysisRunning) {
			try {
				Thread.sleep(THREAD_SLEEP_VALUE);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}
	}
	
	
	private void issueGoInfiniteCommand(String fen) {
		String setFenCommand = "";
		if (!fen.equals("")) {
			setFenCommand = "position fen " + fen + "\n";
		}
		String command = setFenCommand + "go infinite\n";
		issueCommand(command);
	}
	private void issueCommand(String command) {
		try {
			byte[] outBuffer=command.getBytes();
			engineOut.write(outBuffer,0,outBuffer.length);
			engineOut.flush();
		} catch (IOException ex) {
			Logger.getLogger(
					"Error sending the command " + command.getBytes()
							+ "to the engine").log(Level.SEVERE, null, ex);
		}
	}	
	private void startWithEngineCommunication() {
		ProcessBuilder uciEngineProcessBuilder = new ProcessBuilder(uciEngineFullPath);
		try {
			uciEngineProcessBuilder.redirectErrorStream(true);
			Process uciEngineProcess = uciEngineProcessBuilder.start();
			engineIn = uciEngineProcess.getInputStream();
			engineOut = new BufferedOutputStream(uciEngineProcess.getOutputStream());
		} catch (IOException ex) {
			Logger.getLogger("Error loading the engine").log(Level.SEVERE,
					null, ex);

		}
	}	
	
	private void endFirstElaboration(Date dataInizio) {
		stampaPrimoRiepilogo(dataInizio);
	}
	private void stampaPrimoRiepilogo(Date dataInizio) {
		System.out.println("Total games number " + numeroPartiteTotali);
		System.out.println("Total fens number " + numeroFenTotali);
		System.out.println("Total incorrect games "
				+ numeroPartiteInFormatoScorretto);
		System.out.println("Total inserted games " + numeroPartiteInserite);
		Date dataFine = new Date();
		long tempoImpiegato = dataFine.getTime() - dataInizio.getTime();
		long minutiImpiegati = tempoImpiegato / 60000;
		double secondiImpiegati = ((float) tempoImpiegato % 60000) / 1000;
		System.out.println("End First Summary. " + "\r\n"
				+ "Used minutes : " + minutiImpiegati + "\r\n"
				+ "Used seconds : " + secondiImpiegati);
	}
	
	public Boolean isCurrentFenAnalysisRunning() {
		return currentFenAnalysisRunning;
	}

	public void setCurrentFenAnalysisRunning(Boolean currentFenAnalysisRunning) {
		this.currentFenAnalysisRunning = currentFenAnalysisRunning;
	}

	public Thread getEngineReadThread() {
		return engineReadThread;
	}

	public void setEngineReadThread(Thread engineReadThread) {
		this.engineReadThread = engineReadThread;
	}
	class RunnableEngineReadThread implements Runnable {

		private BufferedReader bufferedInputReader;
		private FenResult fenResult;
		private ResultWriter resultWriter ;
		
		public RunnableEngineReadThread(InputStream engine_in, FenResult fenResult) {
			this.bufferedInputReader = new BufferedReader(new InputStreamReader(engine_in));
			this.setFenResult(fenResult);
			this.resultWriter = new ResultWriter();
		}

		@Override
		public void run() {
			int score = Integer.MAX_VALUE;
			while (currentFenAnalysisRunning)// to control
			{
				try {
					String currentLine =  bufferedInputReader.readLine();
					if (currentLine != null) {
						score = resultWriter.getScore(currentLine.trim(),fenResult, score);
					} 

				} catch (IOException ex) {
					Logger.getLogger("Engine read IO exception").log(
							Level.SEVERE, null, ex);
				}
			}

		}

		public FenResult getFenResult() {
			return fenResult;
		}

		public void setFenResult(FenResult fenResult) {
			this.fenResult = fenResult;
		}
	}
	
	class ResultWriter {
		public int getScore(String currentLine,FenResult fenResult, int score) {
			Matcher bestmoveMatcher = getBestMoveMatcher(currentLine);
			if (bestmoveMatcher.find()) {
				System.out.println("Current score " + score);
				currentFenAnalysisRunning = false;
				numeroFenInserite++;
				setResult(fenResult, score);
			}
			Matcher scoreCpMatcher = getScoreCpMatcher(currentLine);
			if (scoreCpMatcher.find()) {
				return Integer.parseInt(scoreCpMatcher.group(2));
			}
			return score;
		}
		private Matcher getScoreCpMatcher(String uci) {
			Pattern getScoreCp = Pattern.compile("(score cp )([^ ]+)");
			Matcher scoreCpMatcher = getScoreCp.matcher(uci);
			return scoreCpMatcher;
		}

		private Matcher getBestMoveMatcher(String uci) {
			Pattern getBestMove = Pattern.compile("(^bestmove )(.*)");
			return getBestMove.matcher(uci);
		}
		

		private void setResult(FenResult fenResult, int score) {
			String fen =fenResult.getFen();
			boolean isBlackMove=false;
			try {
				ChessBoard chessBoard=(ChessBoard)(new FEN().stringToBoard(fen));
				isBlackMove=chessBoard.isBlackMove();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(score > DRAW_THREESHOLD) {
				if(isBlackMove) {
					fenResult.setResult(BLACK_WIN_INT);					
				}
				else {
					fenResult.setResult(WHITE_WIN_INT);
				}
			}
			else
			if(score < -DRAW_THREESHOLD) {
				if(isBlackMove) {
					fenResult.setResult(WHITE_WIN_INT);					
				}
				else {
					fenResult.setResult(BLACK_WIN_INT);
				}
			}
			else {
				fenResult.setResult(DRAW_INT);
			}
		}
	}
		private void startReadThread(FenResult fenResult) {
			RunnableEngineReadThread runnableEngineReadThread = new RunnableEngineReadThread(engineIn,
					fenResult);
			engineReadThread = new Thread(runnableEngineReadThread);
			engineReadThread.start();
		}	
	private void setLatestFensFromPgnAndTruncateGames() {
;
		try {
			setInputGames();
			for(Iterator<ChessGame> it=inputGames.iterator();it.hasNext();){
				ChessGame currentGame=it.next();
				partitaCorrente++;
				System.out.println("Current game: "+partitaCorrente);
				ChessGameInfo chessGameInfo=(ChessGameInfo)currentGame.getGameInfo();
				String ecoCode=chessGameInfo.getECO();
				int cutSemiMove=2*Integer.parseInt((String)(databaseImproverProperties.get(ecoCode)))+41;
				History currentHistory = currentGame.getHistory();
				currentHistory.rewind();
				int semiMoveNumber=0;
				while (currentHistory.hasNext() && semiMoveNumber<= cutSemiMove) {
					currentHistory.next();
					semiMoveNumber++;
				}
				currentHistory.prev();
				ChessMove currentMove=(ChessMove)currentHistory.prev();
				if(currentMove.getBoard().getPlayerToMove()==1) {
					currentMove=(ChessMove)currentHistory.prev();
				}
				currentHistory.truncate();
				FenResult currentFenResult=new FenResult();
				currentFenResult.setFen(fen.boardToString((ChessBoard)currentMove.getBoard()));
				latestFensResults.add(currentFenResult);
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private void setInputGames() throws IOException {
		pgnReader = getPGNReader();
		ChessGame inputGame = getCurrentGame();
		while (inputGame != null) {
			inputGames.add(inputGame);
			inputGame = getCurrentGame();
		}
		closeGamesReader();
	}

	private void closeGamesReader() throws IOException {
		pgnReader.close();
		gamesFr.close();
		gamesInputBr.close();
	}
	
	private PGNReader getPGNReader() throws FileNotFoundException {
		gamesFr = new FileReader(dbFullPath);
		gamesInputBr = new BufferedReader(gamesFr);
		pgnReader = new PGNReader(gamesInputBr);
		return pgnReader;
	}
	
	private ChessGame getCurrentGame() {
		ChessGame currentGame = null;
		while (currentGame == null) {
			try {
				currentGame = (ChessGame) pgnReader.readGame();
				if (currentGame != null) {
					numeroPartiteTotali++;
					System.out.println("Loaded game: " + numeroPartiteTotali);
				} else {
					return null;
				}

			} catch (InvalidGameFormatException | IllegalMoveException
					| AmbiguousMoveException ex) {
				numeroPartiteInFormatoScorretto++;
				System.out.println("Game " + (numeroPartiteTotali + 1)
						+ " has an incorrect pgn.");
			} catch (IOException ioEx) {
				return null;
			}
		}
		return currentGame;
	}
	private static Properties getDatabaseImproverProperties(String[] args) {
		String databaseImproverProperties = args[0];
		Properties properties = new Properties();
		try {
			File file = new File(databaseImproverProperties);
			FileInputStream fileInput = new FileInputStream(file);
			properties.load(fileInput);
			fileInput.close();
	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties;
	}
	public String getUciEngineFullPath() {
		return uciEngineFullPath;
	}
	public int getThreadsNumber() {
		return threadsNumber;
	}
	public int getHashSizeMB() {
		return hashSizeMB;
	}
	public String getSygyzyPath() {
		return sygyzyPath;
	}
	public String getDbFullPath() {
		return dbFullPath;
	}
	public void setUciEngineFullPath(String uciEngineFullPath) {
		this.uciEngineFullPath = uciEngineFullPath;
	}
	public void setThreadsNumber(int threadsNumber) {
		this.threadsNumber = threadsNumber;
	}
	public void setHashSizeMB(int hashSizeMB) {
		this.hashSizeMB = hashSizeMB;
	}
	public int getCpuMhz() {
		return cpuMhz;
	}
	public void setCpuMhz(int cpuMhz) {
		this.cpuMhz = cpuMhz;
	}
	public void setSygyzyPath(String sygyzyPath) {
		this.sygyzyPath = sygyzyPath;
	}
	public void setDbFullPath(String dbFullPath) {
		this.dbFullPath = dbFullPath;
	}
	public String getDbFullPathImproved() {
		return dbFullPathImproved;
	}
	public void setDbFullPathImproved(String dbFullPathImproved) {
		this.dbFullPathImproved = dbFullPathImproved;
	}
	public int getLeveledElo() {
		return leveledElo;
	}
	public void setLeveledElo(int leveledElo) {
		this.leveledElo = leveledElo;
	}
	public long getStrongestAverageTimeSecondsForMove() {
		return strongestAverageTimeSecondsForMove;
	}
	public void setStrongestAverageTimeSecondsForMove(long strongestAverageTimeSecondsForMove) {
		this.strongestAverageTimeSecondsForMove = strongestAverageTimeSecondsForMove;
	}
	public List<FenResult> getLatestFensResults() {
		return latestFensResults;
	}
	public void setLatestFensResults(List<FenResult> latestFensResults) {
		this.latestFensResults = latestFensResults;
	}
}
