
package com.alphachess;

import static com.alphachess.util.Constants.CHESS_LEARNER_ICON_PATH;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;

import com.alphachess.model.dao.impl.LearningFileEntryDaoImpl;
import com.alphachess.model.dao.impl.UciEngineDaoImpl;
import com.alphachess.model.service.LearningFileEntryService;
import com.alphachess.model.service.UciEngineService;
import com.alphachess.model.service.impl.LearningFileEntryServiceImpl;
import com.alphachess.model.service.impl.UciEngineServiceImpl;
import com.alphachess.view.LearningFileEntryEditDialogController;
import com.alphachess.view.RootLayoutController;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainApp extends Application {
	private static final java.util.logging.Logger experienceManagementLogger = java.util.logging.Logger
			.getLogger("experienceManagement.log");

	private static String hashkeyGeneratorFullPath;

	private Stage primaryStage;

	private LearningFileEntryService learningFileEntryService;
	private UciEngineService uciEngineService;

	/**
	 * The data as an observable list of FXlearningFileEntry.
	 * 
	 */
	private ObservableList<com.alphachess.model.fx.FXLearningFileEntry> fxLearningFileEntryData;

	private Properties properties;

	/**
	 * Constructor
	 */
	public MainApp() {
		this.learningFileEntryService = new LearningFileEntryServiceImpl();
		this.uciEngineService = new UciEngineServiceImpl();

		learningFileEntryService.setLearningFileEntryDao(new LearningFileEntryDaoImpl());
		fxLearningFileEntryData = FXCollections.observableArrayList();
		uciEngineService.setUciEngineDao(new UciEngineDaoImpl());
	}

	/**
	 * Returns the data as an observable list of Persons.
	 * 
	 * @return
	 */
	public ObservableList<com.alphachess.model.fx.FXLearningFileEntry> getFXLearningFileEntryData() {
		return fxLearningFileEntryData;
	}

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("ExperienceManagement");

		// Set the application icon.
		this.primaryStage.getIcons().add(new Image(getClass().getResourceAsStream(CHESS_LEARNER_ICON_PATH)));

		initRootLayout();
	}

	/**
	 * Initializes the root layout.
	 */
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
			SplitPane rootLayout = loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);

			// Give the controller access to the main app.
			RootLayoutController controller = loader.getController();
			controller.setMainApp(this);

			primaryStage.show();
		} catch (IOException e) {
			experienceManagementLogger.log(Level.SEVERE, e.getMessage(), e);
		}

	}

	/**
	 * Opens a dialog to edit details for the specified entry. If the user clicks
	 * OK, the changes are saved into the provided entry object and true is
	 * returned.
	 * 
	 * @param fxLearningFileEntry the entry object to be edited
	 * @return true if the user clicked OK, false otherwise.
	 */
	public boolean showLearningFileEntryEditDialog(com.alphachess.model.fx.FXLearningFileEntry fxLearningFileEntry) {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/LearningFileEntryEditDialog.fxml"));
			AnchorPane page = loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edit Learning file's entry");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Set the entry into the controller.
			LearningFileEntryEditDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setFXLearningFileEntry(fxLearningFileEntry);

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();

			return controller.isOkClicked();
		} catch (IOException e) {
			experienceManagementLogger.log(Level.SEVERE, e.getMessage(), e);
			return false;
		}
	}

	/**
	 * Returns the main stage.
	 * 
	 * @return
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public static void main(String[] args) {
		hashkeyGeneratorFullPath = args[0];
		launch(args);
	}

	/**
	 * Loads learning file entries data from the specified file. The current
	 * learning file entry data will be replaced.
	 * 
	 * @param file
	 */
	public void loadLearningFileEntryDataFromFile(File file) {
		try {
			// Reading bin file
			fxLearningFileEntryData.clear();
			fxLearningFileEntryData.addAll(learningFileEntryService.getAll(file.getAbsolutePath()));
		} catch (Exception e) { // catches ANY exception
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Could not load data");
			alert.setContentText("Could not load data from file:\n" + file.getPath());

			alert.showAndWait();
		}
	}

	/**
	 * Saves the current learning file entry data to the specified file.
	 * 
	 * @param file
	 */
	public void saveLearningFileEntryDataToFile(File file) {
		try {
			// writing bin file
			learningFileEntryService.insertAll(file.getAbsolutePath(), fxLearningFileEntryData);
		} catch (Exception e) { // catches ANY exception
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Could not save data");
			alert.setContentText("Could not save data to file:\n" + file.getPath());

			alert.showAndWait();
		}
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public void getHelpContent() {
		InputStream resource = getClass().getResourceAsStream("/help/help.pdf");
		File file = null;
		if (resource == null) {
			return;
		}
		Desktop desktop = Desktop.getDesktop();
		OutputStream out = null;
		try (InputStream resourceIn = resource) {
			file = File.createTempFile("help", ".pdf");
			file.deleteOnExit();
			out = new FileOutputStream(file);
			final int LEN_ARR = 8192;
			final byte[] b = new byte[LEN_ARR];
			for (int r; (r = resource.read(b, 0, LEN_ARR)) != -1;) {
				out.write(b, 0, r);

			}
			desktop.open(file);
		} catch (IOException e) {
			experienceManagementLogger.log(Level.SEVERE, e.getMessage(), e);
		}
		try {
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			experienceManagementLogger.log(Level.SEVERE, e.getMessage(), e);
		}

	}

	public String getHashkeyGeneratorFullPath() {
		return hashkeyGeneratorFullPath;
	}

	public void setHashkeyGeneratorFullPath(String hashkeyGeneratorFullPath) {
		MainApp.hashkeyGeneratorFullPath = hashkeyGeneratorFullPath;
	}

}
