
package com.alphachess.model.dao.impl;

import static com.alphachess.util.GenericUtil.intToByteArray;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.alphachess.model.domain.LearningFileEntry;

public class LearningFileEntryDaoImpl implements com.alphachess.model.dao.LearningFileEntryDao {

	private static final int LAST_HASHKEY_INDEX = 7;

	private static final int LAST_SCORE_INDEX = 15;

	private static final int LAST_MOVE_INDEX = 19;

	private static final int INTEGER_SIZE = 4;

	private static final int BIGINTEGER_SIZE = 8;

	private static final int LAST_DEPTH_INDEX = 11;

	private static final int SCORE_INDEX = 12;

	private static final int HASH_KEY_SIZE_BYTES = 8;

	private static final int SHIFT_PERFORMANCE = 12;

	private static final int SHIFT_MOVE = 8;

	private static final int SHIFT_SCORE = 4;

	private static final int SHIFT_DEPTH = 0;

	public static final int EXPERIENCE_FILE_ENTRY_SIZE_BYTES = 24;

	private static final int LAST_PERFORMANCE_INDEX = 23;

	private static final java.util.logging.Logger experienceManagementLogger = java.util.logging.Logger
			.getLogger("experienceManagement.log");


	@Override
	public com.alphachess.model.domain.LearningFileEntry getByHashKey(List<LearningFileEntry> learningFileEntries,
			BigInteger hashKey) {
		for (LearningFileEntry currentLearningFileEntry : learningFileEntries) {
			if (currentLearningFileEntry.getHashKey().equals(hashKey)) {
				return currentLearningFileEntry;
			}
		}
		return null;
	}

	@Override
	public void createOrUpdate(List<LearningFileEntry> learningFileEntries,
			com.alphachess.model.domain.LearningFileEntry learningFileEntry) {
		if (learningFileEntry.getHashKey() != null) {
			LearningFileEntry existingLearningFileEntry = getByHashKey(learningFileEntries,
					learningFileEntry.getHashKey());
			LearningFileEntry toInsertOrUpdateLearningFileEntry = existingLearningFileEntry != null
					? existingLearningFileEntry
					: new LearningFileEntry();
			if (learningFileEntry.getHashKey() != null) {
				toInsertOrUpdateLearningFileEntry.setDepth(learningFileEntry.getDepth());
				toInsertOrUpdateLearningFileEntry.setMove(learningFileEntry.getMove());
				toInsertOrUpdateLearningFileEntry.setPerformance(learningFileEntry.getPerformance());
				toInsertOrUpdateLearningFileEntry.setScore(learningFileEntry.getScore());
			}
			if (existingLearningFileEntry == null) {
				learningFileEntries.add(toInsertOrUpdateLearningFileEntry);
			}
		}
	}

	@Override
	public void delete(List<LearningFileEntry> learningFileEntries, BigInteger hashKey) {
		LearningFileEntry existingLearningFileEntry = getByHashKey(learningFileEntries, hashKey);
		if (existingLearningFileEntry != null) {
			learningFileEntries.remove(learningFileEntries.indexOf(existingLearningFileEntry));
		}
	}

	@Override
	public void insertAll(String experienceFileFullPath, List<LearningFileEntry> learningFileEntries) {
		File experienceFile = new File(experienceFileFullPath);
		byte[] experienceFileBytes = new byte[learningFileEntries.size() * EXPERIENCE_FILE_ENTRY_SIZE_BYTES];

		try (FileOutputStream fos = new FileOutputStream(experienceFile, false)) {
			insertLearningFileEntries(learningFileEntries, experienceFileBytes);
			fos.write(experienceFileBytes);
			fos.flush();

		} catch (IOException e) {
			experienceManagementLogger.log(Level.SEVERE, e.getMessage(), e);
		}
	}

	@Override
	public List<com.alphachess.model.domain.LearningFileEntry> getAll(String experienceFilePath) {
		File experienceFile = new File(experienceFilePath);
		FileInputStream experienceFileInputStream = null;
		ArrayList<LearningFileEntry> learningFileEntries = new ArrayList<>();
		try {
			experienceFileInputStream = addLearningFileEntries(experienceFile, learningFileEntries);
		} catch (FileNotFoundException e) {
			experienceManagementLogger.log(Level.SEVERE, "File not found", e);
		} catch (IOException ioe) {
			experienceManagementLogger.log(Level.SEVERE, "Exception while reading file ", ioe);
		} finally {
			try {
				if (experienceFileInputStream != null) {
					experienceFileInputStream.close();
				}
			} catch (IOException ioe) {
				experienceManagementLogger.log(Level.SEVERE, "Error while closing stream: ", ioe);
			}
		}
		return learningFileEntries;
	}

	private void insertLearningFileEntries(List<LearningFileEntry> learningFileEntries, byte[] experienceFileBytes) {
		int learningFileEntriesSize = learningFileEntries.size();
		for (int learningFileEntryIndex = 0; learningFileEntryIndex < learningFileEntriesSize; learningFileEntryIndex++) {
			LearningFileEntry currentLearningFileEntry = learningFileEntries.get(learningFileEntryIndex);
			insertHashkey(experienceFileBytes, learningFileEntryIndex, currentLearningFileEntry);
			insertInteger(experienceFileBytes, learningFileEntryIndex, SHIFT_DEPTH,
					currentLearningFileEntry.getDepth());
			insertInteger(experienceFileBytes, learningFileEntryIndex, SHIFT_SCORE,
					currentLearningFileEntry.getScore());
			insertInteger(experienceFileBytes, learningFileEntryIndex, SHIFT_MOVE, currentLearningFileEntry.getMove());
			insertInteger(experienceFileBytes, learningFileEntryIndex, SHIFT_PERFORMANCE,
					currentLearningFileEntry.getPerformance());
			experienceManagementLogger.log(Level.INFO, "Learning file entry {0} inserted", learningFileEntryIndex);
		}
	}

	private void insertInteger(byte[] experienceFileBytes, int learningFileEntryIndex, int shift, int toInsert) {
		byte[] toInsertByteArray = intToByteArray(toInsert);
		for (int j = EXPERIENCE_FILE_ENTRY_SIZE_BYTES * learningFileEntryIndex + HASH_KEY_SIZE_BYTES
				+ shift; j < EXPERIENCE_FILE_ENTRY_SIZE_BYTES * learningFileEntryIndex + SCORE_INDEX + shift; j++) {
			experienceFileBytes[j] = toInsertByteArray[LAST_DEPTH_INDEX + shift - j % EXPERIENCE_FILE_ENTRY_SIZE_BYTES];
		}
	}

	private void insertHashkey(byte[] experienceFileBytes, int learningFileEntryIndex,
			com.alphachess.model.domain.LearningFileEntry currentLearningFileEntry) {
		BigInteger hashKey = currentLearningFileEntry.getHashKey();
		byte[] hashKeyByteArray = hashKey.toByteArray();
		for (int j = EXPERIENCE_FILE_ENTRY_SIZE_BYTES * learningFileEntryIndex; j < EXPERIENCE_FILE_ENTRY_SIZE_BYTES
				* learningFileEntryIndex + hashKeyByteArray.length; j++) {
			experienceFileBytes[j] = hashKeyByteArray[hashKeyByteArray.length - j % EXPERIENCE_FILE_ENTRY_SIZE_BYTES
					- 1];
		}
	}

	private FileInputStream addLearningFileEntries(File experienceFile,
			ArrayList<LearningFileEntry> learningFileEntries) throws IOException {
		try (FileInputStream experienceFileInputStream = new FileInputStream(experienceFile)) {

			int experienceFileSize = (int) experienceFile.length();
			int experienceFileEntriesNumber = experienceFileSize / EXPERIENCE_FILE_ENTRY_SIZE_BYTES;
			byte[] inputExperienceFileBytes = new byte[experienceFileSize];
			while (experienceFileInputStream.read(inputExperienceFileBytes) > 0) {
				for (int experienceFileEntryNumber = 0; experienceFileEntryNumber < experienceFileEntriesNumber; experienceFileEntryNumber++) {
					addLearningFileEntry(learningFileEntries, inputExperienceFileBytes, experienceFileEntryNumber);
				}
			}
			return experienceFileInputStream;
		}
	}

	private void addLearningFileEntry(ArrayList<LearningFileEntry> learningFileEntries, byte[] inputExperienceFileBytes,
			int experienceFileEntryNumber) {
		byte[] currentHashKey = new byte[BIGINTEGER_SIZE];
		byte[] currentDepth = new byte[INTEGER_SIZE];
		byte[] currentScore = new byte[INTEGER_SIZE];
		byte[] currentMove = new byte[INTEGER_SIZE];
		byte[] currentPerformance = new byte[INTEGER_SIZE];
		LearningFileEntry currentLearningFileEntry = new LearningFileEntry();
		setHashKey(inputExperienceFileBytes, experienceFileEntryNumber, currentHashKey, currentLearningFileEntry);
		setDepth(inputExperienceFileBytes, experienceFileEntryNumber, currentDepth, currentLearningFileEntry);
		setScore(inputExperienceFileBytes, experienceFileEntryNumber, currentScore, currentLearningFileEntry);
		setMove(inputExperienceFileBytes, experienceFileEntryNumber, currentMove, currentLearningFileEntry);
		setPerformance(inputExperienceFileBytes, experienceFileEntryNumber, currentPerformance,
				currentLearningFileEntry);
 		learningFileEntries.add(currentLearningFileEntry);
	}

	private void setPerformance(byte[] inputExperienceFileBytes, int experienceFileEntryNumber,
			byte[] currentPerformance, com.alphachess.model.domain.LearningFileEntry currentLearningFileEntry) {
		for (int i = 0; i < INTEGER_SIZE; i++) {
			currentPerformance[i] = inputExperienceFileBytes[EXPERIENCE_FILE_ENTRY_SIZE_BYTES
					* experienceFileEntryNumber + (LAST_PERFORMANCE_INDEX - i)];
		}
		currentLearningFileEntry.setPerformance(ByteBuffer.wrap(currentPerformance).getInt());
	}

	private void setMove(byte[] inputExperienceFileBytes, int experienceFileEntryNumber, byte[] currentMove,
			com.alphachess.model.domain.LearningFileEntry currentLearningFileEntry) {
		for (int i = 0; i < INTEGER_SIZE; i++) {
			currentMove[i] = inputExperienceFileBytes[EXPERIENCE_FILE_ENTRY_SIZE_BYTES * experienceFileEntryNumber
					+ (LAST_MOVE_INDEX - i)];
		}
		currentLearningFileEntry.setMove(ByteBuffer.wrap(currentMove).getInt());
	}

	private void setScore(byte[] inputExperienceFileBytes, int experienceFileEntryNumber, byte[] currentScore,
			com.alphachess.model.domain.LearningFileEntry currentLearningFileEntry) {
		for (int i = 0; i < INTEGER_SIZE; i++) {
			currentScore[i] = inputExperienceFileBytes[EXPERIENCE_FILE_ENTRY_SIZE_BYTES * experienceFileEntryNumber
					+ (LAST_SCORE_INDEX - i)];
		}
		currentLearningFileEntry.setScore(ByteBuffer.wrap(currentScore).getInt());
	}

	private void setDepth(byte[] inputExperienceFileBytes, int experienceFileEntryNumber, byte[] currentDepth,
			com.alphachess.model.domain.LearningFileEntry currentLearningFileEntry) {
		for (int i = 0; i < INTEGER_SIZE; i++) {
			currentDepth[i] = inputExperienceFileBytes[EXPERIENCE_FILE_ENTRY_SIZE_BYTES * experienceFileEntryNumber
					+ (LAST_DEPTH_INDEX - i)];
		}
		currentLearningFileEntry.setDepth(ByteBuffer.wrap(currentDepth).getInt());
	}

	private void setHashKey(byte[] inputExperienceFileBytes, int experienceFileEntryNumber, byte[] currentHashKey,
			com.alphachess.model.domain.LearningFileEntry currentLearningFileEntry) {
		for (int i = 0; i < BIGINTEGER_SIZE; i++) {
			currentHashKey[i] = inputExperienceFileBytes[EXPERIENCE_FILE_ENTRY_SIZE_BYTES * experienceFileEntryNumber
					+ (LAST_HASHKEY_INDEX - i)];
		}
		currentLearningFileEntry.setHashKey(new BigInteger(1, currentHashKey));
	}
}
