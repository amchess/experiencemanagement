package com.alphachess.model.dao.impl;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.alphachess.model.dao.UciEngineDao;
import com.alphachess.util.GenericUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UciEngineDaoImpl implements UciEngineDao {
	private static final int FEN_PROCESSING_THREAD_SLEEP_TIME = 250;
	private String uciEnginePath;
	private InputStream engineIn;
	private BufferedOutputStream engineOut;
	private Thread engineReadThread;
	private Boolean currentFenAnalysisRunning = false;
	HashMap<String, String> resultKeyFenHashMap;
	private static final java.util.logging.Logger experienceManagementLogger = java.util.logging.Logger
			.getLogger("experienceManagement.log");
	ObjectMapper mapper = new ObjectMapper();
	@Override
	public HashMap<String, String> getFensListInHashKeyFenHashMap(List<String> fens, String uciEngineAbsolutePath,
			File mapFile) {
		initKeyFenHashMap(mapFile);
		if((fens!=null) && (!fens.isEmpty())){
			uciEnginePath = uciEngineAbsolutePath;
			startWithEngineCommunication();
			for (String currentFen : fens) {
				if (!currentFen.equals("")) {
					process(currentFen);
					setCurrentFenAnalysisRunning(false);
				}
			}

			issueCommand("quit\n");
			getEngineReadThread().interrupt();
			fens.clear();
			
		}
		return resultKeyFenHashMap;
	}

	private void initKeyFenHashMap(File mapFile) {
		if ((mapFile != null) && mapFile.exists()) {
			try {
				resultKeyFenHashMap= (HashMap<String, String>) mapper.readValue(mapFile, new TypeReference<Map<String, String>>() {
				});
			} catch (IOException e) {
				experienceManagementLogger.log(Level.SEVERE, e.getMessage(), e);
			}
		}
		else {
			resultKeyFenHashMap=new HashMap<>();
		}
		
		mergeEventualOtherMapFiles(mapFile);
	}

	private void mergeEventualOtherMapFiles(File mapFile) {
		int fileIndex=0;
		while(true) {
			File mapFileFileIndex=GenericUtil.getFile(mapFile, fileIndex);
			if(mapFileFileIndex!=null && mapFileFileIndex.exists() &&(mapFileFileIndex.length()>0)) {
				try {
					HashMap<String, String> fileIndexKeyFenHashMap= (HashMap<String, String>) mapper.readValue(mapFileFileIndex, new TypeReference<Map<String, String>>() {
					});
			        // using for-each loop for iteration over Map.entrySet() 
			        for (Map.Entry<String,String> currentEntry : fileIndexKeyFenHashMap.entrySet()) {
			        	if(!resultKeyFenHashMap.containsKey(currentEntry.getKey())) {
			        		resultKeyFenHashMap.put(currentEntry.getKey(), currentEntry.getValue());
			        	}
			        }
			        Files.deleteIfExists(Paths.get(mapFileFileIndex.getAbsolutePath()));
				} catch (IOException e) {
					experienceManagementLogger.log(Level.SEVERE, e.getMessage(), e);
				}				
			}
			else {
				break;
			}
		}
	}

	private void startWithEngineCommunication() {
		ProcessBuilder uciEngineProcessBuilder = new ProcessBuilder(uciEnginePath);
		try {
			uciEngineProcessBuilder.redirectErrorStream(true);
			Process uciEngineProcess = uciEngineProcessBuilder.start();
			engineIn = uciEngineProcess.getInputStream();
			engineOut = new BufferedOutputStream(uciEngineProcess.getOutputStream());
		} catch (IOException ex) {
			Logger.getLogger("Error loading the engine").log(Level.SEVERE, null, ex);

		}
	}

	private void process(String fen) {
		setCurrentFenAnalysisRunning(true);
		startReadThread(fen);
		issuePositionFenCommand(fen);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			experienceManagementLogger.log(Level.SEVERE, e.getMessage(), e);
			Thread.currentThread().interrupt();
		}
		issueCommand("stop\n");
		while (Boolean.TRUE.equals(currentFenAnalysisRunning)) {
			try {
				Thread.sleep(FEN_PROCESSING_THREAD_SLEEP_TIME);
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
			}
		}
	}

	private void startReadThread(String fen) {
		RunnableEngineReadThread runnableEngineReadThread = new RunnableEngineReadThread(engineIn, fen);
		engineReadThread = new Thread(runnableEngineReadThread);
		engineReadThread.start();
	}

	private void issuePositionFenCommand(String fen) {
		String setFenCommand = "";
		if (!fen.equals("")) {
			setFenCommand = "position fen " + fen + "\n";
		}
		issueCommand(setFenCommand);
	}

	private void issueCommand(String command) {
		try {
			byte[] outBuffer = command.getBytes();
			engineOut.write(outBuffer, 0, outBuffer.length);
			engineOut.flush();
		} catch (IOException ex) {
			experienceManagementLogger.log(Level.SEVERE, "Error sending the command " + command.getBytes() + "to the engine",
					ex);
		}
	}

	public Boolean isCurrentFenAnalysisRunning() {
		return currentFenAnalysisRunning;
	}

	public void setCurrentFenAnalysisRunning(Boolean currentFenAnalysisRunning) {
		this.currentFenAnalysisRunning = currentFenAnalysisRunning;
	}

	public Thread getEngineReadThread() {
		return engineReadThread;
	}

	public void setEngineReadThread(Thread engineReadThread) {
		this.engineReadThread = engineReadThread;
	}

	class RunnableEngineReadThread implements Runnable {

		private BufferedReader bufferedInputReader;
		private String currentFen;

		public RunnableEngineReadThread(InputStream engineIn, String fen) {
			this.bufferedInputReader = new BufferedReader(new InputStreamReader(engineIn));
			this.setFen(fen);
		}

		@Override
		public void run() {
			String currentHashKey = "";
			while (Boolean.TRUE.equals(currentFenAnalysisRunning))// to control
			{
				try {
					String currentLine = bufferedInputReader.readLine();
					if ((currentLine != null) && (!currentLine.trim().equals(""))
							&& (!currentLine.contains("HashKeyGenerator"))
							&& (!currentLine.contains("Unknown command"))) {
						currentHashKey = currentLine.trim();
						if (!resultKeyFenHashMap.containsValue(currentFen)) {
							resultKeyFenHashMap.put(currentHashKey, currentFen);
							experienceManagementLogger.log(Level.INFO, "Inserted entry for key  {0}",currentHashKey);
						}
						currentFenAnalysisRunning = false;
					}

				} catch (IOException ex) {
					Logger.getLogger("Engine read IO exception").log(Level.SEVERE, null, ex);
				}
			}
		}

		public String getFen() {
			return currentFen;
		}

		public void setFen(String fen) {
			this.currentFen = fen;
		}
	}
}
