package com.alphachess.model.dao;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface UciEngineDao {

	Map<String,String> getFensListInHashKeyFenHashMap(List<String> fens, String uciEngineAbsolutePath, File mapFile);

}
