
package com.alphachess.model.dao;

import java.math.BigInteger;
import java.util.List;

import com.alphachess.model.domain.LearningFileEntry;

public interface LearningFileEntryDao {
	com.alphachess.model.domain.LearningFileEntry getByHashKey(List<LearningFileEntry> learningFileEntries,
			BigInteger hashKey);

	void createOrUpdate(List<LearningFileEntry> learningFileEntries,
			com.alphachess.model.domain.LearningFileEntry learningFileEntry);

	void delete(List<LearningFileEntry> learningFileEntries, BigInteger hashKey);

	void insertAll(String experienceFileFullPath, List<LearningFileEntry> learningFileEntries);

	List<com.alphachess.model.domain.LearningFileEntry> getAll(String experienceFilePath);
}
