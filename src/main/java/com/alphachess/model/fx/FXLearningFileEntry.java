
package com.alphachess.model.fx;

import static com.alphachess.util.GenericUtil.getAlgebricMove;
import static com.alphachess.util.GenericUtil.getIntegerPropertyFromInteger;
import static com.alphachess.util.GenericUtil.getStringPropertyFromString;

import com.alphachess.util.GenericUtil;

import ictk.boardgame.chess.ChessBoard;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;

public class FXLearningFileEntry {
	private StringProperty positionIdentifier;

	private IntegerProperty depth;

	private IntegerProperty score;

	private StringProperty move;
	
	private IntegerProperty moveInt;

	private IntegerProperty performance;

	public FXLearningFileEntry(String positionIdentifier, Integer depth, Integer score, Integer move,
			Integer performance) {
		this(getStringPropertyFromString(positionIdentifier), getIntegerPropertyFromInteger(depth),
				getIntegerPropertyFromInteger(score), getStringPropertyFromString(getAlgebricMove(move)),getIntegerPropertyFromInteger(move), 
				getIntegerPropertyFromInteger(performance));
	}
	public FXLearningFileEntry(String positionIdentifier, Integer depth, Integer score, String move,
			Integer performance,ChessBoard currentChessBoard) {
		this(getStringPropertyFromString(positionIdentifier), getIntegerPropertyFromInteger(depth),
				getIntegerPropertyFromInteger(score), getStringPropertyFromString(move),getIntegerPropertyFromInteger(GenericUtil.getIntMove(move,currentChessBoard)),
				getIntegerPropertyFromInteger(performance));
	}
	public FXLearningFileEntry(StringProperty positionIdentifierProperty, IntegerProperty depthProperty,
			IntegerProperty scoreProperty, StringProperty moveProperty, IntegerProperty moveIntProperty, IntegerProperty performanceProperty) {
		super();
		this.positionIdentifier = positionIdentifierProperty;
		this.depth = depthProperty;
		this.score = scoreProperty;
		this.move = moveProperty;
		this.moveInt=moveIntProperty;
		this.performance = performanceProperty;
	}

	public String getPositionIdentifier() {
		return positionIdentifier.get();
	}

	public void setPositionIdentifier(String positionIdentifier) {
		this.positionIdentifier.set(positionIdentifier);
	}

	public StringProperty positionIdentifierProperty() {
		return positionIdentifier;
	}

	public int getDepth() {
		return depth.get();
	}

	public void setDepth(int depth) {
		this.depth.set(depth);
	}

	public IntegerProperty depthProperty() {
		return depth;
	}

	public int getScore() {
		return score.get();
	}

	public void setScore(int score) {
		this.score.set(score);
	}

	public IntegerProperty scoreProperty() {
		return score;
	}

	public String getMove() {
		return move.get();
	}

	public void setMove(String move) {
		this.move.set(move);
	}
	
	public StringProperty moveProperty() {
		return move;
	}

	public int getMoveInt() {
		return moveInt.get();
	}

	public void setMoveInt(int moveInt) {
		this.moveInt.set(moveInt);
	}

	public IntegerProperty moveIntProperty() {
		return moveInt;
	}

	public int getPerformance() {
		return performance.get();
	}

	public void setPerformance(int performance) {
		this.performance.set(performance);
	}

	public IntegerProperty performanceProperty() {
		return performance;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FXLearningFileEntry [");
		if (positionIdentifier != null) {
			builder.append("positionIdentifier=");
			builder.append(positionIdentifier.get());
			builder.append(", ");
		}
		if (depth != null) {
			builder.append("depth=");
			builder.append(depth.get());
			builder.append(", ");
		}
		if (score != null) {
			builder.append("score=");
			builder.append(score.get());
			builder.append(", ");
		}
		if (move != null) {
			builder.append("move=");
			builder.append(move.get());
			builder.append(", ");
		}
		if (moveInt != null) {
			builder.append("moveInt=");
			builder.append(moveInt.get());
			builder.append(", ");
		}
		if (performance != null) {
			builder.append("performance=");
			builder.append(performance.get());
		}
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((depth == null) ? 0 : Integer.hashCode(depth.get()));
		result = prime * result + ((move == null) ? 0 : move.get().hashCode());
		result = prime * result + ((moveInt == null) ? 0 : Integer.hashCode(moveInt.get()));
		result = prime * result + ((performance == null) ? 0 : performance.hashCode());
		result = prime * result + ((positionIdentifier == null) ? 0 : positionIdentifier.get().hashCode());
		result = prime * result + ((score == null) ? 0 : Integer.hashCode(score.get()));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (differentObj(obj)) {
			return false;
		}
		FXLearningFileEntry other = (FXLearningFileEntry) obj;
		return (!differentDepth(other) && !differentMove(other) && !differentMoveInt(other) && !differentPerformance(other)
				&& !differentPositionIdentifier(other) && !differentScore(other));
	}

	private boolean differentScore(FXLearningFileEntry other) {
		return ((score == null) && (other.score != null)) || ((score != null) && (score.get() != other.score.get()));
	}

	private boolean differentPositionIdentifier(FXLearningFileEntry other) {
		return ((positionIdentifier == null) && (other.positionIdentifier != null))
				|| ((positionIdentifier != null) && (!positionIdentifier.get().equals(other.positionIdentifier.get())));
	}

	private boolean differentPerformance(FXLearningFileEntry other) {
		return ((performance == null) && (other.performance != null))
				|| ((performance != null) && (performance.get() != other.performance.get()));
	}

	private boolean differentMove(FXLearningFileEntry other) {
		return ((move == null) && (other.move != null)) || ((move != null) && (!move.get().equals(other.move.get())));
	}

	private boolean differentMoveInt(FXLearningFileEntry other) {
		return ((moveInt == null) && (other.moveInt != null))
				|| ((moveInt != null) && (moveInt.get() != other.moveInt.get()));
	}
	
	private boolean differentDepth(FXLearningFileEntry other) {
		return ((depth == null) && (other.depth != null)) || ((depth != null) && (depth.get() != other.depth.get()));
	}

	private boolean differentObj(Object obj) {
		return (obj == null) || (getClass() != obj.getClass());
	}

}
