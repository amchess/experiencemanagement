package com.alphachess.model.fx;

import java.util.HashMap;
import java.util.Map;

import com.alphachess.view.design.BoardRenderer;
import com.alphachess.view.design.game.Coordinate;
import com.alphachess.view.design.game.Piece;
import com.alphachess.view.design.game.PieceType;
import com.alphachess.view.design.game.Player;

import javafx.scene.canvas.Canvas;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public final class FXPieceTypeCellFactory implements Callback<ListView<PieceType>, ListCell<PieceType>> {

    private final BoardRenderer renderer;
    private final Player player;
    private final Coordinate coordinate;
    private final Map<PieceType, Canvas> images = new HashMap<>();

    public FXPieceTypeCellFactory(BoardRenderer renderer, Player player, Coordinate coordinate) {
        this.renderer = renderer;
        this.player = player;
        this.coordinate = coordinate;
    }

    private Canvas createPieceImage(PieceType pieceType) {
        Piece piece = Piece.valueOf(pieceType, this.player);
        Canvas canvas = new Canvas();
        canvas.setWidth(100);
        canvas.setHeight(100);
        this.renderer.drawSquare(canvas.getGraphicsContext2D(), 100, this.coordinate, piece,
                false, false, false, null);
        return canvas;
    }

    @Override
    public ListCell<PieceType> call(ListView<PieceType> param) {
        return new ListCell<PieceType>() {

            public void updateItem(PieceType pieceType, boolean empty) {
                super.updateItem(pieceType, empty);
                setText(null);
                if (empty) {
                    setGraphic(null);
                } else {
                    setGraphic(images.computeIfAbsent(pieceType, p -> createPieceImage(pieceType)));
                }

            }
        };
    }
}
