package com.alphachess.model.fx;

import java.util.List;
import java.util.Optional;

import com.alphachess.util.CurrentStatus;
import com.alphachess.util.GenericUtil;
import com.alphachess.view.PromotionController;
import com.alphachess.view.RootLayoutController;
import com.alphachess.view.design.BoardDesign;
import com.alphachess.view.design.BoardRenderer;
import com.alphachess.view.design.ClassicBoardRenderer;
import com.alphachess.view.design.game.CheckState;
import com.alphachess.view.design.game.Coordinate;
import com.alphachess.view.design.game.Game;
import com.alphachess.view.design.game.MoveDirection;
import com.alphachess.view.design.game.PieceType;
import com.alphachess.view.design.game.Player;
import com.alphachess.view.design.game.Ply;
import com.alphachess.view.design.game.PlyType;

import ictk.boardgame.AmbiguousMoveException;
import ictk.boardgame.IllegalMoveException;
import ictk.boardgame.chess.Bishop;
import ictk.boardgame.chess.ChessBoard;
import ictk.boardgame.chess.ChessMove;
import ictk.boardgame.chess.Knight;
import ictk.boardgame.chess.Queen;
import ictk.boardgame.chess.Rook;
import ictk.boardgame.chess.Square;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyListWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public final class FXBoardCanvas extends Canvas {

	private static final class Dimensions {

		final double xOffset;
		final double yOffset;
		final double borderSize;
		final double squareSize;

		Dimensions(double width, double height) {
			double prefSquareSize = 100;
			double prefBorderSize = 30;
			double prefBoardWidth = 2 * prefBorderSize + Coordinate.COLUMNS * prefSquareSize;
			double prefBoardHeight = 2 * prefBorderSize + Coordinate.ROWS * prefSquareSize;

			double widthScale = width / prefBoardWidth;
			double heightScale = height / prefBoardHeight;
			double scale = Math.min(widthScale, heightScale);

			this.xOffset = (width - (scale * prefBoardWidth)) / 2;
			this.yOffset = (height - (scale * prefBoardHeight)) / 2;

			this.borderSize = scale * prefBorderSize;
			this.squareSize = scale * prefSquareSize;
		}
	}

	private final Tooltip tooltip;

	private Game game;
	private BoardRenderer renderer;

	private final ReadOnlyObjectWrapper<Player> activePlayerProperty = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyObjectWrapper<CheckState> checkStateProperty = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyBooleanWrapper undoAvailable = new ReadOnlyBooleanWrapper();
	private final ReadOnlyBooleanWrapper redoAvailable = new ReadOnlyBooleanWrapper();
	private final ObjectProperty<Coordinate> selectedSquareProperty = new SimpleObjectProperty<>();
	private final ObjectProperty<Coordinate> focusedSquareProperty = new SimpleObjectProperty<>();
	private final ReadOnlyListWrapper<Ply> displayedPliesProperty = new ReadOnlyListWrapper<>(
			FXCollections.observableArrayList());

	private CurrentStatus currentStatus = new CurrentStatus();
	private RootLayoutController rootLayoutController;

	ictk.boardgame.chess.io.FEN fen = new ictk.boardgame.chess.io.FEN();

	public FXBoardCanvas(RootLayoutController rootLayoutController) {
		this.game = new Game();
		this.rootLayoutController = rootLayoutController;
		setFocusTraversable(true);

		this.tooltip = new Tooltip();
		// this.tooltip.setShowDelay(Duration.ZERO);
		this.tooltip.setOnShowing(e -> tooltipShowing());

		setOnMouseMoved(this::mouseMoved);
		setOnMousePressed(this::mousePressed);
		setOnKeyPressed(this::keyPressed);

		widthProperty().addListener((observable, oldValue, newValue) -> draw());
		heightProperty().addListener((observable, oldValue, newValue) -> draw());
		focusedProperty().addListener((observable, oldValue, newValue) -> draw());

		this.selectedSquareProperty.addListener((observable, oldValue, newValue) -> draw());
		this.selectedSquareProperty.addListener((observable, oldValue, newValue) -> updateDisplayedPlies());
		this.focusedSquareProperty.addListener((observable, oldValue, newValue) -> draw());
		this.displayedPliesProperty.addListener((observable, oldValue, newValue) -> draw());

		updateProperties();
	}

	public Game getGame() {
		return this.game;
	}

	public void setGame(Game game) {
		this.game = game;
		this.focusedSquareProperty.set(null);
		this.selectedSquareProperty.set(null);
		updateProperties();
		draw();
	}

	public void setDesign(BoardDesign design) {
		this.renderer = new ClassicBoardRenderer();
		draw();
	}

	public ReadOnlyObjectProperty<Player> activePlayerProperty() {
		return this.activePlayerProperty.getReadOnlyProperty();
	}

	public ReadOnlyObjectProperty<CheckState> checkStateProperty() {
		return this.checkStateProperty.getReadOnlyProperty();
	}

	ReadOnlyBooleanProperty undoAvailableProperty() {
		return this.undoAvailable.getReadOnlyProperty();
	}

	ReadOnlyBooleanProperty redoAvailableProperty() {
		return this.redoAvailable.getReadOnlyProperty();
	}

	public ObjectProperty<Coordinate> selectedSquareProperty() {
		return this.selectedSquareProperty;
	}

	public void undo() {
		this.game.undo();
		this.selectedSquareProperty.set(null);
		updateProperties();
		draw();
	}

	public void redo() {
		this.game.redo();
		this.selectedSquareProperty.set(null);
		updateProperties();
		draw();
	}

	private void tooltipShowing() {
		Point2D pos = localToScreen(0, 0);
		this.tooltip.setX(pos.getX());
		this.tooltip.setY(pos.getY());
	}

	private void mouseMoved(MouseEvent e) {
		Coordinate coordinate = getSquareAt(e.getX(), e.getY());
		if (coordinate != null) {
			Optional<Ply> ply = this.displayedPliesProperty.stream()
					.filter(m -> coordinate.equals(m.target) || coordinate.equals(m.captures)).findFirst();
			if (ply.isPresent()) {
				tooltip.setText(ply.get().getDisplayName());
				Tooltip.install(this, tooltip);
			} else {
				this.tooltip.setText("");
				Tooltip.uninstall(this, tooltip);
			}
			this.focusedSquareProperty.set(coordinate);
		} else {
			this.tooltip.setText("");
			Tooltip.uninstall(this, tooltip);
			this.focusedSquareProperty.set(null);
		}
	}

	private void mousePressed(MouseEvent e) {
		if (!isFocused())
			requestFocus();

		Coordinate square = getSquareAt(e.getX(), e.getY());
		if (square != null)
			selectSquare(square, "", false);
	}

	private void keyPressed(KeyEvent e) {
		Coordinate focused = this.focusedSquareProperty.get();
		Coordinate selected = this.selectedSquareProperty.get();

		switch (e.getCode()) {
		case LEFT:
			if (focused != null) {
				Coordinate left = focused.go(MoveDirection.LEFT);
				if (left != null)
					this.focusedSquareProperty.set(left);
				else
					this.focusedSquareProperty.set(Coordinate.valueOf(Coordinate.COLUMNS - 1, focused.rowIndex));
			} else
				this.focusedSquareProperty.set(Coordinate.a1);
			e.consume();
			break;
		case RIGHT:
			if (focused != null) {
				Coordinate right = focused.go(MoveDirection.RIGHT);
				if (right != null)
					this.focusedSquareProperty.set(right);
				else
					this.focusedSquareProperty.set(Coordinate.valueOf(0, focused.rowIndex));
			} else
				this.focusedSquareProperty.set(Coordinate.a1);
			e.consume();
			break;
		case UP:
			if (focused != null) {
				Coordinate up = focused.go(MoveDirection.UP);
				if (up != null)
					this.focusedSquareProperty.set(up);
				else
					this.focusedSquareProperty.set(Coordinate.valueOf(focused.columnIndex, 0));
			} else
				this.focusedSquareProperty.set(Coordinate.a1);
			e.consume();
			break;
		case DOWN:
			if (focused != null) {
				Coordinate down = focused.go(MoveDirection.DOWN);
				if (down != null)
					this.focusedSquareProperty.set(down);
				else
					this.focusedSquareProperty.set(Coordinate.valueOf(focused.columnIndex, Coordinate.ROWS - 1));
			} else
				this.focusedSquareProperty.set(Coordinate.a1);
			e.consume();
			break;
		case ENTER:
			if (focused != null) {
				selectSquare(focused, "", false);
			}
			break;
		case ESCAPE:
			if (selected != null)
				this.selectedSquareProperty.set(null);
			else if (focused != null)
				this.focusedSquareProperty.set(null);
			e.consume();
			break;
		default:
			break;
		}
	}

	public void selectSquare(Coordinate coordinate, String promotionPiece, boolean fromExperienceTable) {
		// check if we can execute a go
		Optional<Ply> ply = this.displayedPliesProperty.stream()
				.filter(m -> coordinate.equals(m.target) || coordinate.equals(m.captures)).findFirst();

		if (ply.isPresent()) {
			performPly(ply.get(), promotionPiece, fromExperienceTable);
		} else {
			this.selectedSquareProperty.set(coordinate);
		}
	}

	private void performPly(Ply ply, String promotionPiece, boolean fromExperienceTable) {
		PieceType promotion = null;
		if ((ply.type == PlyType.PAWN_PROMOTION) || (promotionPiece.trim().length() == 1)) {
			if (ply.type == PlyType.PAWN_PROMOTION) {
				promotion = PromotionController.showDialog(this, this.renderer, ply);
			} else {
				switch (promotionPiece) {
				case "q":
					promotion = PieceType.QUEEN;
					break;
				case "r":
					promotion = PieceType.ROOK;
					break;
				case "n":
					promotion = PieceType.KNIGHT;
					break;
				case "b":
					promotion = PieceType.BISHOP;
					break;
				}
			}
			if (promotion != null)
			{
				ply.promotion = promotion;
			} else {
				return;
			}
		}

		this.game.perform(ply);

		this.selectedSquareProperty.set(null);
		this.focusedSquareProperty.set(ply.target);
		updateProperties();
		if(!fromExperienceTable) {
			String origSquareStr=ply.source.name();
			Square origSquare = currentStatus.getCurrentChessBoard().getSquare((byte) (origSquareStr.charAt(0) - 96),
					Byte.parseByte(origSquareStr.substring(1)));
			String destSquareStr = ply.target.name();
			ChessBoard currentChessBoard=currentStatus.getCurrentChessBoard();
			Square destSquare = currentChessBoard.getSquare((byte) (destSquareStr.charAt(0) - 96),
					Byte.parseByte(destSquareStr.substring(1)));
			try {
				ChessMove currentChessMove = new ChessMove(currentChessBoard, origSquare, destSquare);
				currentChessMove = GenericUtil.getCastleMove(currentStatus,origSquareStr, destSquareStr, currentChessMove);
				currentChessMove = getPromotionMove(promotion, origSquareStr+destSquareStr, promotionPiece, currentChessMove);
				try {
					currentStatus.getCurrentHistory().add(currentChessMove);
					currentStatus.setCurrentChessBoardFen(fen.boardToString(currentChessBoard));
					ObservableMap<String, String> currentObservableMap = rootLayoutController.getCurrentObservableMap();
					if(currentObservableMap!=null && currentObservableMap.keySet()!=null) {
						currentStatus.setCurrentPositionIdentifier((String)currentObservableMap.keySet().toArray()[0]);
					}
					try {
						rootLayoutController.setExperienceTableViewItems();
					} catch (SecurityException | IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (AmbiguousMoveException | IndexOutOfBoundsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (IllegalMoveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//TODO
			
		}
	}

	private ChessMove getPromotionMove(PieceType promotion, String move, String promotionPiece,
			ChessMove currentChessMove) throws IllegalMoveException {
		if (promotion != null) {
			int promo = 0;
			switch (promotionPiece) {
			case "q":
				promo = Queen.INDEX;
				break;
			case "r":
				promo = Rook.INDEX;
				break;
			case "b":
				promo = Bishop.INDEX;
				break;
			case "n":
				promo = Knight.INDEX;
				break;
			default:
				break;
			}
			currentChessMove = new ChessMove(currentStatus.getCurrentChessBoard(), ((int) move.charAt(0) - 96),
					(Character.getNumericValue(move.charAt(1))), ((int) move.charAt(2) - 96),
					(Character.getNumericValue(move.charAt(3))), promo);
		}
		return currentChessMove;
	}

	private void updateProperties() {
		this.activePlayerProperty.set(this.game.getActivePlayer());
		this.checkStateProperty.set(this.game.getCheckState());
		this.undoAvailable.set(this.game.isUndoAvailable());
		this.redoAvailable.set(this.game.isRedoAvailable());
	}

	private Coordinate getSquareAt(double x, double y) {
		if (this.renderer != null) {
			Dimensions dim = new Dimensions(getWidth(), getHeight());
			int columnIndex = (int) Math.floor((x - dim.xOffset - dim.borderSize) / dim.squareSize);
			int rowIndex = (int) Math.floor(Coordinate.ROWS - (y - dim.yOffset - dim.borderSize) / dim.squareSize);
			if (columnIndex >= 0 && columnIndex < Coordinate.COLUMNS && rowIndex >= 0 && rowIndex < Coordinate.ROWS) {
				return Coordinate.valueOf(columnIndex, rowIndex);
			}
		}
		return null;
	}

	private void draw() {
		double width = getWidth();
		double height = getHeight();

		// clear the drawing
		GraphicsContext gc = getGraphicsContext2D();
		gc.clearRect(0, 0, width, height);

		if (this.renderer == null)
			return;

		// calculate the dimensions
		Dimensions dim = new Dimensions(width, height);

		// draw the canvas background
		this.renderer.drawBackground(gc, width, height);

		// draw vertical borders
		double xLeftBorder = dim.xOffset;
		double xRightBorder = dim.xOffset + dim.borderSize + (Coordinate.COLUMNS * dim.squareSize);
		for (int rowIndex = 0; rowIndex < Coordinate.ROWS; rowIndex++) {
			double yBorder = dim.yOffset + dim.borderSize + ((Coordinate.ROWS - rowIndex - 1) * dim.squareSize);

			// left border
			gc.save();
			gc.translate(xLeftBorder, yBorder);
			this.renderer.drawBorder(gc, dim.borderSize, dim.squareSize, BoardRenderer.Border.LEFT, rowIndex,
					isFocused());
			gc.restore();

			// right border
			gc.save();
			gc.translate(xRightBorder, yBorder);
			this.renderer.drawBorder(gc, dim.borderSize, dim.squareSize, BoardRenderer.Border.RIGHT, rowIndex,
					isFocused());
			gc.restore();
		}

		// draw the horizontal borders
		double yTopBorder = dim.yOffset;
		double yBottomBorder = dim.yOffset + dim.borderSize + (Coordinate.ROWS * dim.squareSize);
		for (int columnIndex = 0; columnIndex < Coordinate.COLUMNS; columnIndex++) {
			double xBorder = dim.xOffset + dim.borderSize + (columnIndex * dim.squareSize);
			// top border
			gc.save();
			gc.translate(xBorder, yTopBorder);
			this.renderer.drawBorder(gc, dim.squareSize, dim.borderSize, BoardRenderer.Border.TOP, columnIndex,
					isFocused());
			gc.restore();

			// bottom border
			gc.save();
			gc.translate(xBorder, yBottomBorder);
			this.renderer.drawBorder(gc, dim.squareSize, dim.borderSize, BoardRenderer.Border.BOTTOM, columnIndex,
					isFocused());
			gc.restore();
		}

		// draw top-left corner
		gc.save();
		gc.translate(xLeftBorder, yTopBorder);
		this.renderer.drawCorner(gc, dim.borderSize, BoardRenderer.Corner.TOP_LEFT, isFocused());
		gc.restore();

		// draw top-right corner
		gc.save();
		gc.translate(xRightBorder, yTopBorder);
		this.renderer.drawCorner(gc, dim.borderSize, BoardRenderer.Corner.TOP_RIGHT, isFocused());
		gc.restore();

		// draw bottom-left corner
		gc.save();
		gc.translate(xLeftBorder, yBottomBorder);
		this.renderer.drawCorner(gc, dim.borderSize, BoardRenderer.Corner.BOTTOM_LEFT, isFocused());
		gc.restore();

		// draw bottom-right corner
		gc.save();
		gc.translate(xRightBorder, yBottomBorder);
		this.renderer.drawCorner(gc, dim.borderSize, BoardRenderer.Corner.BOTTOM_RIGHT, isFocused());
		gc.restore();

		// draw the squares
		for (Coordinate coordinate : Coordinate.values()) {

			double squareXOffset = dim.borderSize + dim.xOffset + (coordinate.columnIndex * dim.squareSize);
			double squareYOffset = dim.borderSize + (dim.yOffset + ((Coordinate.ROWS - 1) * dim.squareSize))
					- (coordinate.rowIndex * dim.squareSize);

			gc.save();
			gc.translate(squareXOffset, squareYOffset);

			BoardRenderer.SquareIndicator squareIndicator;
			if (this.displayedPliesProperty.stream().anyMatch(m -> coordinate.equals(m.captures))) {
				squareIndicator = BoardRenderer.SquareIndicator.CAPTURED;
			} else if (this.displayedPliesProperty.stream().anyMatch(m -> coordinate.equals(m.target))) {
				squareIndicator = BoardRenderer.SquareIndicator.TARGET;
			} else {
				squareIndicator = null;
			}

			this.renderer.drawSquare(gc, dim.squareSize, coordinate, this.game.getPiece(coordinate), isFocused(),
					coordinate.equals(this.focusedSquareProperty.get()),
					coordinate.equals(this.selectedSquareProperty.get()), squareIndicator);
			gc.restore();
		}
	}

	private void updateDisplayedPlies() {
		this.displayedPliesProperty.clear();
		Coordinate selected = this.selectedSquareProperty.get();
		if (selected != null) {
			List<Ply> plies = this.game.getValidPlies(selected);
			if (!plies.isEmpty())
				this.displayedPliesProperty.addAll(plies);
		}
	}

	public CurrentStatus getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(CurrentStatus currentStatus) {
		this.currentStatus = currentStatus;
	}
}
