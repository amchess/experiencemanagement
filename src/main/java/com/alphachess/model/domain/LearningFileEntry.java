
package com.alphachess.model.domain;

import java.math.BigInteger;
import java.util.Arrays;

public class LearningFileEntry {
  private BigInteger hashKey;
 
  private int depth;

  private int score;

  private int move;

  private int performance =  0;

  public LearningFileEntry() {
		super();
  }

  public LearningFileEntry(BigInteger hashKey, int depth, int score, int move, int performance) {
		this();
		this.hashKey = hashKey;
		this.depth = depth;
		this.score = score;
		this.move = move;
		this.performance = performance;
  }

  public BigInteger getHashKey() {
		return hashKey;
  }

  public void setHashKey(BigInteger hashKey) {
		this.hashKey = hashKey;
  }

  public int getDepth() {
		return depth;
  }

  public void setDepth(int depth) {
		this.depth = depth;
  }

  public int getScore() {
		return score;
  }

  public void setScore(int score) {
		this.score = score;
  }

  public int getMove() {
		return move;
  }

  public void setMove(int move) {
		this.move = move;
  }

  public int getPerformance() {
		return performance;
  }

  public void setPerformance(int performance) {
		this.performance = performance;
  }

@Override
  public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + depth;
		result = prime * result + ((hashKey == null) ? 0 : hashKey.hashCode());
		result = prime * result + move;
		result = prime * result + performance;
		result = prime * result + score;
		return result;
  }

  @Override
  public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		final LearningFileEntry other = (LearningFileEntry) obj;
		return Arrays.equals(new Object[] { this.depth, this.hashKey, this.move, this.performance, this.score },
				new Object[] { other.depth, other.hashKey, other.move, other.performance, other.score });
  }

  @Override
  public String toString() {
		return "LearningFileEntry [hashKey=" + hashKey + ", depth=" + depth + ", score=" + score + ", move=" + move
				+ ", performance=" + performance + "]";
  }

}
