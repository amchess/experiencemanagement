package com.alphachess.model.service;

import java.io.File;
import java.util.List;

import com.alphachess.model.dao.UciEngineDao;

import javafx.collections.ObservableMap;

public interface UciEngineService {

	void setUciEngineDao(UciEngineDao uciEngineDaoMock);

	ObservableMap<String, String> getFensListInHashKeyFenHashMap(List<String> fens, String uciEngineAbsolutePath, File mapFile);

}
