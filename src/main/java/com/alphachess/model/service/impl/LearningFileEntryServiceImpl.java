
package com.alphachess.model.service.impl;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import com.alphachess.model.domain.LearningFileEntry;
import com.alphachess.model.fx.FXLearningFileEntry;
import com.alphachess.util.GenericUtil;

import ictk.boardgame.chess.ChessBoard;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class LearningFileEntryServiceImpl implements com.alphachess.model.service.LearningFileEntryService {

	private com.alphachess.model.dao.LearningFileEntryDao learningFileEntryDao;
	private static ChessBoard currentChessBoard;

	public void setLearningFileEntryDao(com.alphachess.model.dao.LearningFileEntryDao learningFileEntryDao) {
		this.learningFileEntryDao = learningFileEntryDao;
	}

	public void createOrUpdate(ObservableList<FXLearningFileEntry> fxLearningFileEntries,
			com.alphachess.model.fx.FXLearningFileEntry fxLearningFileEntry) {
		List<LearningFileEntry> learningFileEntries = getLearningFileEntries(fxLearningFileEntries,currentChessBoard);
		LearningFileEntry learningFileEntry = getLearningFileEntry(fxLearningFileEntry);
		learningFileEntryDao.createOrUpdate(learningFileEntries, learningFileEntry);
	}

	public ObservableList<com.alphachess.model.fx.FXLearningFileEntry> getAll(String experienceFilePath) {
		return getFXLearningFileEntries(learningFileEntryDao.getAll(experienceFilePath));
	}

	public com.alphachess.model.fx.FXLearningFileEntry getByHashKey(
			ObservableList<FXLearningFileEntry> fxLearningFileEntries, StringProperty positionIdentifier) {
		BigInteger hashKey = new BigInteger(positionIdentifier.getValue());
		return getFXLearningFileEntry(
				learningFileEntryDao.getByHashKey(getLearningFileEntries(fxLearningFileEntries,currentChessBoard), hashKey));
	}

	public void insertAll(String experienceFileFullPath, ObservableList<FXLearningFileEntry> fxLearningFileEntries,ChessBoard currentChessBoard) {
		List<LearningFileEntry> learningFileEntries = getLearningFileEntries(fxLearningFileEntries,currentChessBoard);
		learningFileEntryDao.insertAll(experienceFileFullPath, learningFileEntries);
	}
	public void insertAll(String experienceFileFullPath, ObservableList<FXLearningFileEntry> fxLearningFileEntries) {
		List<LearningFileEntry> learningFileEntries = getLearningFileEntries(fxLearningFileEntries);
		learningFileEntryDao.insertAll(experienceFileFullPath, learningFileEntries);
	}
	private static com.alphachess.model.fx.FXLearningFileEntry getFXLearningFileEntry(
			com.alphachess.model.domain.LearningFileEntry learningFileEntry) {
		String posIdentifier = GenericUtil.getPosIdentifier(learningFileEntry);
		return new FXLearningFileEntry(posIdentifier, learningFileEntry.getDepth(), learningFileEntry.getScore(),
				learningFileEntry.getMove(), learningFileEntry.getPerformance());
	}

	private static com.alphachess.model.domain.LearningFileEntry getLearningFileEntry(
			com.alphachess.model.fx.FXLearningFileEntry fxLearningFileEntry) {
		return new LearningFileEntry(new BigInteger(fxLearningFileEntry.getPositionIdentifier()),
				fxLearningFileEntry.getDepth(), fxLearningFileEntry.getScore(),
				GenericUtil.getIntMove(fxLearningFileEntry.getMove(),currentChessBoard), fxLearningFileEntry.getPerformance());
	}
	private static com.alphachess.model.domain.LearningFileEntry getLearningFileEntryNotFromChessBoard(
			com.alphachess.model.fx.FXLearningFileEntry fxLearningFileEntry) {
		return new LearningFileEntry(new BigInteger(fxLearningFileEntry.getPositionIdentifier()),
				fxLearningFileEntry.getDepth(), fxLearningFileEntry.getScore(),
				fxLearningFileEntry.getMoveInt(), fxLearningFileEntry.getPerformance());
	}
	private ObservableList<com.alphachess.model.fx.FXLearningFileEntry> getFXLearningFileEntries(
			List<LearningFileEntry> learningFileEntries) {
		return learningFileEntries.stream().map(LearningFileEntryServiceImpl::getFXLearningFileEntry)
				.collect(Collectors.toCollection(FXCollections::observableArrayList));
	}
	
	@SuppressWarnings("static-access")
	private List<com.alphachess.model.domain.LearningFileEntry> getLearningFileEntries(
			ObservableList<FXLearningFileEntry> fxLearningFileEntries) {
		return fxLearningFileEntries.stream().map(LearningFileEntryServiceImpl::getLearningFileEntryNotFromChessBoard)
				.collect(Collectors.toList());
	}
	
	@SuppressWarnings("static-access")
	private List<com.alphachess.model.domain.LearningFileEntry> getLearningFileEntries(
			ObservableList<FXLearningFileEntry> fxLearningFileEntries,ChessBoard currentChessBoard) {
		this.currentChessBoard=currentChessBoard;
		return fxLearningFileEntries.stream().map(LearningFileEntryServiceImpl::getLearningFileEntry)
				.collect(Collectors.toList());
	}

	public ChessBoard getCurrentChessBoard() {
		return currentChessBoard;
	}

	@SuppressWarnings("static-access")
	public void setCurrentChessBoard(ChessBoard currentChessBoard) {
		this.currentChessBoard = currentChessBoard;
	}
}
