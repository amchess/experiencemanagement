package com.alphachess.model.service.impl;

import java.io.File;
import java.util.List;

import com.alphachess.model.dao.UciEngineDao;
import com.alphachess.model.dao.impl.UciEngineDaoImpl;
import com.alphachess.model.service.UciEngineService;

import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

public class UciEngineServiceImpl implements UciEngineService {
	private com.alphachess.model.dao.UciEngineDao uciEngineDao=new UciEngineDaoImpl();
	
	public void setUciEngineDao(UciEngineDao uciEngineDao) {
		this.uciEngineDao = uciEngineDao;
	}
	public ObservableMap<String, String> getFensListInHashKeyFenHashMap(List<String> fens, String uciEngineAbsolutePath, File mapFile){
		return FXCollections.observableMap(uciEngineDao.getFensListInHashKeyFenHashMap(fens,uciEngineAbsolutePath, mapFile));
	}
}
