
package com.alphachess.model.service;

import com.alphachess.model.fx.FXLearningFileEntry;

import ictk.boardgame.chess.ChessBoard;
import javafx.collections.ObservableList;

public interface LearningFileEntryService {
	void createOrUpdate(ObservableList<FXLearningFileEntry> fxLearningFileEntries,
			com.alphachess.model.fx.FXLearningFileEntry fxLearningFileEntry);

	ObservableList<com.alphachess.model.fx.FXLearningFileEntry> getAll(String experienceFilePath);

	void insertAll(String experienceFileFullPath, ObservableList<FXLearningFileEntry> fxLearningFileEntries,ChessBoard currentChessBoard);

	void insertAll(String experienceFileFullPath, ObservableList<FXLearningFileEntry> fxLearningFileEntries);
	
	void setLearningFileEntryDao(com.alphachess.model.dao.LearningFileEntryDao learningFileEntryDao);

}
