package com.alphachess.util;

import ictk.boardgame.History;
import ictk.boardgame.chess.ChessBoard;
import ictk.boardgame.chess.ChessGame;

public class CurrentStatus {
	private ChessBoard currentChessBoard;

	private ChessGame currentChessGame;

	private History currentHistory;
	
	private String currentChessBoardFen = "";

	private String currentPositionIdentifier;
	
	public ChessBoard getCurrentChessBoard() {
		return currentChessBoard;
	}

	public void setCurrentChessBoard(ChessBoard currentChessBoard) {
		this.currentChessBoard = currentChessBoard;
	}

	public ChessGame getCurrentChessGame() {
		return currentChessGame;
	}

	public void setCurrentChessGame(ChessGame currentChessGame) {
		this.currentChessGame = currentChessGame;
	}

	public History getCurrentHistory() {
		return currentHistory;
	}

	public void setCurrentHistory(History currentHistory) {
		this.currentHistory = currentHistory;
	}

	public String getCurrentChessBoardFen() {
		return currentChessBoardFen;
	}

	public void setCurrentChessBoardFen(String currentChessBoardFen) {
		this.currentChessBoardFen = currentChessBoardFen;
	}

	public String getCurrentPositionIdentifier() {
		return currentPositionIdentifier;
	}

	public void setCurrentPositionIdentifier(String currentPositionIdentifier) {
		this.currentPositionIdentifier = currentPositionIdentifier;
	}
}
