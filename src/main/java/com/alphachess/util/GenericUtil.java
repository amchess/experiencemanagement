package com.alphachess.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import com.alphachess.model.domain.LearningFileEntry;

import ictk.boardgame.AmbiguousMoveException;
import ictk.boardgame.History;
import ictk.boardgame.IllegalMoveException;
import ictk.boardgame.Move;
import ictk.boardgame.chess.AmbiguousChessMoveException;
import ictk.boardgame.chess.Bishop;
import ictk.boardgame.chess.ChessBoard;
import ictk.boardgame.chess.ChessGame;
import ictk.boardgame.chess.ChessMove;
import ictk.boardgame.chess.ChessPiece;
import ictk.boardgame.chess.Knight;
import ictk.boardgame.chess.Queen;
import ictk.boardgame.chess.Rook;
import ictk.boardgame.chess.Square;
import ictk.boardgame.chess.io.SAN;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class GenericUtil {
	private GenericUtil() {
		throw new IllegalStateException("Utility class");
	}

	public static final int MAX_PLY = 246;
	public static final int VALUE_DRAW = 0;
	public static final int KING_POS = 0;
	public static final int ROOK_POS = 1;
	public static final int KNIGHT_POS = 2;
	public static final int BISHOP_POS = 3;
	public static final int QUEEN_POS = 4;
	public static final int PAWN_POS = 5;
	public static final int MOVE_NONE = 0, MOVE_NULL = 65;

	private static final java.util.logging.Logger experienceManagementLogger = java.util.logging.Logger
			.getLogger("experienceManagement.log");

	public enum KingPosition {
		K(1), C(2), Q(4);

		private int value;

		private KingPosition(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	public static File getFile(File file, int fileIndex) {
		if (file != null) {
			String absolutePath = file.getAbsolutePath();
			String newAbsolutePath = absolutePath.replace(".", fileIndex + ".");
			return new File(newAbsolutePath);
		}
		return null;
	}

	public static byte[] intToByteArray(int data) {
		byte[] result = new byte[4];
		result[0] = (byte) ((data & 0xFF000000) >> 24);
		result[1] = (byte) ((data & 0x00FF0000) >> 16);
		result[2] = (byte) ((data & 0x0000FF00) >> 8);
		result[3] = (byte) ((data & 0x000000FF) >> 0);
		return result;
	}

	public static ChessMove getCastleMove(ChessBoard iterationChessBoard, String origSquareStr, String destSquareStr,
			ChessMove iterationChessMove) throws IllegalMoveException {
		if (iterationChessMove == null) {
			iterationChessMove = getCastleMove(origSquareStr, destSquareStr, iterationChessMove, iterationChessBoard);
		}
		return iterationChessMove;
	}

	public static ChessMove getCastleMove(CurrentStatus currentStatus, String origSquareStr, String destSquareStr,
			ChessMove currentChessMove) throws IllegalMoveException {
		ChessBoard currentChessBoard = currentStatus.getCurrentChessBoard();
		ChessPiece whiteOccupant = currentChessBoard.getSquare(5, 1).getOccupant();
		boolean isKingWhite = whiteOccupant != null ? whiteOccupant.isKing() : false;
		ChessPiece blackOccupant = currentChessBoard.getSquare(5, 8).getOccupant();
		boolean isKingBlack = blackOccupant != null ? blackOccupant.isKing() : false;
		if ((isKingWhite && origSquareStr.equals("e1") && destSquareStr.equals("g1"))
				|| (isKingBlack && origSquareStr.equals("e8") && destSquareStr.equals("g8"))) {
			try {
				currentChessMove = (ChessMove) (new SAN().stringToMove(currentChessBoard, "O-O"));
			} catch (AmbiguousChessMoveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if ((isKingWhite && origSquareStr.equals("e1") && destSquareStr.equals("c1"))
				|| (isKingBlack && origSquareStr.equals("e8") && destSquareStr.equals("c8"))) {
			try {
				currentChessMove = (ChessMove) (new SAN().stringToMove(currentChessBoard, "O-O-O"));
			} catch (AmbiguousChessMoveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return currentChessMove;
	}

	public static ChessMove getCastleMove(String origSquareStr, String destSquareStr, ChessMove currentChessMove,
			ChessBoard iterationChessBoard) throws IllegalMoveException {
		ChessPiece whiteOccupant = iterationChessBoard.getSquare(5, 1).getOccupant();
		boolean isKingWhite = whiteOccupant != null ? whiteOccupant.isKing() : false;
		ChessPiece blackOccupant = iterationChessBoard.getSquare(5, 8).getOccupant();
		boolean isKingBlack = blackOccupant != null ? blackOccupant.isKing() : false;
		if ((isKingWhite && origSquareStr.equals("e1") && destSquareStr.equals("g1"))
				|| (isKingBlack && origSquareStr.equals("e8") && destSquareStr.equals("g8"))) {
			try {
				currentChessMove = (ChessMove) (new SAN().stringToMove(iterationChessBoard, "O-O"));
			} catch (AmbiguousChessMoveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if ((isKingWhite && origSquareStr.equals("e1") && destSquareStr.equals("c1"))
				|| (isKingBlack && origSquareStr.equals("e8") && destSquareStr.equals("c8"))) {
			try {
				currentChessMove = (ChessMove) (new SAN().stringToMove(iterationChessBoard, "O-O-O"));
			} catch (AmbiguousChessMoveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return currentChessMove;
	}

	public static StringProperty getStringPropertyFromString(String stringValue) {
		return (new SimpleStringProperty(stringValue));
	}

	public static IntegerProperty getIntegerPropertyFromInteger(Integer intValue) {
		return (new SimpleIntegerProperty(intValue));
	}

	public static BooleanProperty getBooleanPropertyFromBoolean(Boolean boolValue) {
		return (new SimpleBooleanProperty(boolValue));
	}

	public static FloatProperty getFloatPropertyFromFloat(Float floatValue) {
		return (new SimpleFloatProperty(floatValue));
	}

	public static <T> ObjectProperty<T> getObjectPropertyFromObject(T object) {
		return (new SimpleObjectProperty<T>(object));
	}

	public static List<Integer> getIntegersList(List<IntegerProperty> integerPropertiesList) {
		List<Integer> integersList = new ArrayList<>();
		for (IntegerProperty currentIntegerProperty : integerPropertiesList) {
			integersList.add(currentIntegerProperty.get());
		}
		return integersList;
	}

	public static ListProperty<IntegerProperty> getIntegerPropertiesList(List<Byte> bytesList) {
		ListProperty<IntegerProperty> integerProperties = new SimpleListProperty<>();
		if (bytesList != null) {
			for (Byte currByte : bytesList) {
				integerProperties.add(getIntegerPropertyFromInteger((int) currByte));
			}
		}
		return integerProperties;
	}

	public static String getPosIdentifier(LearningFileEntry lfe) {
		return lfe.getHashKey().toString();
	}

	enum MoveType {
		NORMAL(0), PROMOTION(1 << 14), EN_PASSANT(2 << 14), CASTLING(3 << 14);

		public final int moveType;

		private MoveType(int moveType) {
			this.moveType = moveType;
		}

		public static MoveType valueOfMoveType(int moveType) {
			for (MoveType mt : values()) {
				if (mt.moveType == moveType) {
					return mt;
				}
			}
			return null;
		}
	};

	public static MoveType type_of(int moveInt) {
		return MoveType.valueOfMoveType(moveInt & (3 << 14));
	}

	public enum Column {
		FILE_A(0), FILE_B(1), FILE_C(2), FILE_D(3), FILE_E(4), FILE_F(5), FILE_G(6), FILE_H(7), FILE_NB(8);

		private final int value;

		Column(final int newValue) {
			value = newValue;
		}

		public int getValue() {
			return value;
		}
	}

	public enum Rank {
		RANK_A(0), RANK_B(1), RANK_C(2), RANK_D(3), RANK_E(4), RANK_F(5), RANK_G(6), RANK_H(7), RANK_NB(8);

		private final int value;

		Rank(final int newValue) {
			value = newValue;
		}

		public int getValue() {
			return value;
		}
	}

	public static int make_square(int f, int r) {
		return ((r << 3) + f);
	}

	public static int rank_of(int s) {
		return (s >> 3);
	}

	public static int file_of(int s) {
		return (s & 7);
	}

	/// square() converts a int Square to a string in algebraic notation (g1, a7,
	/// etc.)

	public static String square(int s) {
		char firstColumn = 'a';
		char column = (char) ((int) firstColumn + file_of(s));
		char firstRank = '1';
		char rank = (char) ((int) firstRank + rank_of(s));

		return "" + column + rank;
	}

	public static int promotion_type(int moveInt) {
		return (((moveInt >> 12) & 3) + 2);
	}
	/// getAlgebricMove converts a Move to a string in coordinate notation (g1f3,
	/// a7a8q).
	/// The only special case is castling, where we print in the e1g1 notation in
	/// normal chess mode, and in e1h1 notation in chess960 mode. Internally all
	/// castling moves are always encoded as 'king captures rook'.

	public static String getAlgebricMove(int moveInt) {

		int from = getFromSquare(moveInt);
		int to = getToSquare(moveInt);

		if (moveInt == MOVE_NONE)
			return "(none)";

		if (moveInt == MOVE_NULL)
			return "0000";

		if (type_of(moveInt) == MoveType.CASTLING)
			to = make_square(to > from ? Column.FILE_G.getValue() : Column.FILE_C.getValue(), rank_of(from));

		String move = square(from) + square(to);

		if (type_of(moveInt) == MoveType.PROMOTION) {
			String unitsInitial = " pnbrqk";
			char[] unitsInitialCharacters = new char[unitsInitial.length()];
			for (int i = 0; i < unitsInitial.length(); i++) {
				unitsInitialCharacters[i] = unitsInitial.charAt(i);
			}
			move += unitsInitialCharacters[promotion_type(moveInt)];
		}

		return move;
	}

	public static String getAlgebricSquare(int squareInt) {
		return Character.toString((char) ((squareInt % 8) + 97)).concat(Integer.toString((squareInt / 8) + 1));
	}

	public static int getIntSquare(String algebricSquare) {
		Square square = new Square((byte) (algebricSquare.charAt(0) - 96), Byte.parseByte(algebricSquare.substring(1)));
		return getSquareInt(square);
	}

	public static int getFromSquare(int move) {
		return ((move >> 6) & 0x3F);
	}

	public static Square getSquare(int intSquare) {
		String algebricSquare = getAlgebricSquare(intSquare);
		return new Square((byte) (algebricSquare.charAt(0) - 96), Byte.parseByte(algebricSquare.substring(1)));
	}

	public static int getToSquare(int move) {
		return (move & 0x3F);
	}

	public static int getMoveInt(int fromSquare, int toSquare) {
		return (64 * fromSquare + toSquare);
	}

	public static int getMoveInt(Square fromSquare, Square toSquare) {
		return getMoveInt(getSquareInt(fromSquare), getSquareInt(toSquare));
	}

	public static int getSquareInt(Square square) {
		return (8 * square.getRank() - 9 + square.getFile());
	}

	public static String cleanFEN(String fen) {
        // Define a regular expression pattern with allowed characters in a FEN string
        String validFENPattern = "[rnbqkpRNBQKP12345678/wbKQkq\\-a-h36\\d ]";
        
        // Use a StringBuilder to build the cleaned FEN string
        StringBuilder cleanedFEN = new StringBuilder();
        if(fen!=null) {
            // Loop through each character in the input FEN string
            for (char c : fen.toCharArray()) {
                // Append only valid characters that match the defined pattern
                if (String.valueOf(c).matches(validFENPattern)) {
                    cleanedFEN.append(c);
                }
            }
        }
        
        // Return the cleaned FEN string
        return cleanedFEN.toString();
    }
	
	public static int getIntMove(String algebricMove, ChessBoard currentChessBoard) {

		// case castle begin
		boolean isCastle = false;
		String algebricOrigSquare = algebricMove.substring(0, 2);
		String algebricDestSquare = algebricMove.substring(2, 4);
		if (algebricOrigSquare.equals("e1")) {
			if (algebricDestSquare.equals("g1")) {
				algebricMove = algebricMove.replace("g1", "h1");
				isCastle = true;
			}
			if (algebricDestSquare.equals("c1")) {
				algebricMove = algebricMove.replace("c1", "a1");
				isCastle = true;
			}
		}
		if (algebricOrigSquare.equals("e8")) {
			if (algebricDestSquare.equals("g8")) {
				algebricMove = algebricMove.replace("g8", "h8");
				isCastle = true;
			}
			if (algebricDestSquare.equals("c8")) {
				algebricMove = algebricMove.replace("c8", "a8");
				isCastle = true;
			}
		}
		// case castle end
		int fromColStr = algebricMove.charAt(0);
		int fromColInt = fromColStr - 96;
		int fromRowInt = Integer.parseInt(algebricMove.substring(1, 2));
		int fromSquareInt = 8 * fromRowInt + fromColInt - 9;
		int toColStr = algebricMove.charAt(2);
		int toColInt = toColStr - 96;
		int toRowInt = Integer.parseInt(algebricMove.substring(3, 4));
		int toSquareInt = 8 * toRowInt + toColInt - 9;
		int moveInt = getMoveInt(fromSquareInt, toSquareInt);
		String base2Move = Integer.toString(moveInt, 2);
		for (int charNumber = base2Move.length(); charNumber < 16; charNumber++) {
			base2Move = "0" + base2Move;
		}
		StringBuilder base2MoveSB = new StringBuilder(base2Move);

		setEnPassant(currentChessBoard, fromColInt, fromRowInt, toColInt, toRowInt, base2MoveSB);
		setCastle(isCastle, base2MoveSB);
		setPromotion(algebricMove, base2MoveSB);
		return Integer.parseInt(base2Move, 2);
	}
	public static final List<Move> getLegalMoves(ChessBoard chessBoard) {
		List<Move> legalMoves=chessBoard.getLegalMoves();
		List<Move> legalMovesFixed=new ArrayList<>();
		for(Move currentMove:legalMoves) {
			ChessMove currentChessMove=(ChessMove)currentMove;
			Square destinationSquare=currentChessMove.getDestination();
			Square originSquare=currentChessMove.getOrigin();
			byte originRank=originSquare.getRank();
			byte originFile=originSquare.getFile();
			byte destinationRank=destinationSquare.getRank();
			boolean isBlackMove=chessBoard.isBlackMove();
			ChessPiece chessPiece= chessBoard.getSquare(originFile, originRank).getOccupant();
			if(chessPiece!=null) {
				if(chessPiece.isPawn()&& (((destinationRank==8)&&(!isBlackMove))||((destinationRank==1)&&(isBlackMove)))){
					byte destinationFile=destinationSquare.getFile();
					try {
						ChessMove queenMove = new ChessMove(chessBoard, originFile, originRank, destinationFile, destinationRank, Queen.INDEX);
						legalMovesFixed.add(queenMove);
						ChessMove rookMove = new ChessMove(chessBoard, originFile, originRank, destinationFile, destinationRank, Rook.INDEX);
						legalMovesFixed.add(rookMove);
						ChessMove bishopMove = new ChessMove(chessBoard, originFile, originRank, destinationFile, destinationRank, Bishop.INDEX);
						legalMovesFixed.add(bishopMove);
						ChessMove knightMove = new ChessMove(chessBoard, originFile, originRank, destinationFile, destinationRank, Knight.INDEX);
						legalMovesFixed.add(knightMove);
					} catch (IllegalMoveException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else {
					legalMovesFixed.add(currentMove);
				}
			}
		}
		return legalMovesFixed;
	}
	private static void setEnPassant(ChessBoard currentChessBoard, int fromColInt, int fromRowInt, int toColInt,
			int toRowInt, StringBuilder base2MoveSB) {
		List<Move> moves = GenericUtil.getLegalMoves(currentChessBoard);
		ChessGame currentChessGame = new ChessGame();
		currentChessGame.setBoard(currentChessBoard);
		History currentHistory = currentChessGame.getHistory();
		for (Move currentMove : moves) {
			ChessMove currentChessMove = (ChessMove) currentMove;
			try {
				currentHistory.add(currentChessMove);
			} catch (IllegalMoveException | AmbiguousMoveException | IndexOutOfBoundsException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ChessPiece chessPiece = currentChessMove.getChessPiece();
			if (chessPiece.isPawn()) {
				Square currentOriginSquare = currentChessMove.getOrigin();
				int currentOriginSquareFile = currentOriginSquare.getFile();
				int currentOriginSquareRank = currentOriginSquare.getRank();
				Square currentDestinationSquare = currentChessMove.getDestination();
				int currentDestinationSquareFile = currentDestinationSquare.getFile();
				int currentDestinationSquareRank = currentDestinationSquare.getRank();
				if ((currentOriginSquareFile == fromColInt) && (currentOriginSquareRank == fromRowInt)
						&& (currentDestinationSquareFile == toColInt) && (currentDestinationSquareRank == toRowInt)) {
					if (currentChessBoard.getEnPassantFile() == currentDestinationSquareFile) {
						if ((Math.abs(fromRowInt - toRowInt) == 1) && (Math.abs(fromColInt - toColInt) == 1)) {
							base2MoveSB.setCharAt(0, '1');
							base2MoveSB.setCharAt(1, '0');
						}
					}
				}
			}
			currentHistory.removeLastMove();
		}
	}

	private static void setCastle(boolean isCastle, StringBuilder base2MoveSB) {
		if (isCastle) {
			base2MoveSB.setCharAt(0, '1');
			base2MoveSB.setCharAt(1, '1');
		}
	}

	private static void setPromotion(String algebricMove, StringBuilder base2MoveSB) {
		if (algebricMove.length() == 5) {
			base2MoveSB.setCharAt(0, '0');
			base2MoveSB.setCharAt(1, '1');
			String pieceType = algebricMove.substring(4, 5);
			switch (pieceType) {
			case "q":
				base2MoveSB.setCharAt(2, '1');
				base2MoveSB.setCharAt(3, '1');
				break;
			case "r":
				base2MoveSB.setCharAt(2, '1');
				base2MoveSB.setCharAt(3, '0');
				break;
			case "b":
				base2MoveSB.setCharAt(2, '0');
				base2MoveSB.setCharAt(3, '1');
				break;
			case "n":
				base2MoveSB.setCharAt(2, '0');
				base2MoveSB.setCharAt(3, '0');
				break;
			}
		}
	}

	public static int getMoveInt(ChessMove chessMove) {
		if (chessMove != null) {
			Square fromSquare = chessMove.getOrigin();
			Square toSquare = chessMove.getDestination();
			if ((fromSquare != null) && (toSquare != null)) {
				return getMoveInt(fromSquare, toSquare);
			}
		}
		return 0;
	}

	public static int getChessPieceIndex(ChessPiece chessPiece) {
		if (chessPiece.isKing()) {
			return KING_POS;
		}
		if (chessPiece.isRook()) {
			return ROOK_POS;
		}
		if (chessPiece.isKnight()) {
			return KNIGHT_POS;
		}
		if (chessPiece.isBishop()) {
			return BISHOP_POS;
		}
		if (chessPiece.isQueen()) {
			return QUEEN_POS;
		}
		return PAWN_POS;
	}

	@SafeVarargs
	public static <T> List<T> asList(T... elements) {
		if (elements.length == 0)
			return new ArrayList<>();
		return new ArrayList<>(Arrays.asList(elements));
	}

	public static Properties getProperties(String propertiesResourceFileName) {
		Properties prop = new Properties();
		try (InputStream inputStream = ClassLoader.getSystemResourceAsStream(propertiesResourceFileName)) {
			prop.load(inputStream);
		} catch (IOException ex) {
			experienceManagementLogger.log(Level.SEVERE, ex.getMessage(), ex);
		}
		return prop;
	}

	public static void showAlert(String title, String headerText, String contentText, Stage stage) {
		// Nothing selected.
		Alert alert = new Alert(AlertType.WARNING);
		alert.initOwner(stage);
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		alert.setContentText(contentText);
		alert.showAndWait();
	}
}
