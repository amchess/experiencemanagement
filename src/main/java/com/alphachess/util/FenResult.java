package com.alphachess.util;

public class FenResult {
	private String fen;
	private int result;
	public String getFen() {
		return fen;
	}
	public int getResult() {
		return result;
	}
	public void setFen(String fen) {
		this.fen = fen;
	}
	public void setResult(int result) {
		this.result = result;
	}
	

}
