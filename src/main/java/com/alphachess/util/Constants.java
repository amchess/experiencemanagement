package com.alphachess.util;

import java.math.BigInteger;

public final class Constants {
	public static final String CHESS_LEARNER_ICON_PATH = "/images/experiencemanagement_32.png";
	public static final String INITIAL_FEN="rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
	public static final BigInteger INITIAL_FEN_HASHKEY=BigInteger.valueOf(5398946861044308550L);
	public static final String TEST_FEN = "r1bq1rk1/ppp1npbp/3p1np1/3Pp3/2P1P3/2N5/PP2BPPP/R1BQNRK1 b - - 2 9";
	public static final BigInteger TEST_FEN_HASHKEY=BigInteger.valueOf(1237056646505243082L);
	private Constants() {
		
	}

}