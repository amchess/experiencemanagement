package com.alphachess.view;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import com.alphachess.model.fx.FXPieceTypeCellFactory;
import com.alphachess.util.GUIUtils;
import com.alphachess.view.design.BoardRenderer;
import com.alphachess.view.design.game.Coordinate;
import com.alphachess.view.design.game.Messages;
import com.alphachess.view.design.game.PieceType;
import com.alphachess.view.design.game.Player;
import com.alphachess.view.design.game.Ply;
import com.alphachess.view.design.game.Rules;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;

public final class PromotionController implements Initializable {

    public static PieceType showDialog(Node owner, BoardRenderer renderer, Ply ply) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(PromotionController.class.getResource("promotion.fxml"));
        loader.setResources(ResourceBundle.getBundle(Messages.BUNDLE_NAME));
        Parent pane;
        try {
            pane = loader.load();
        } catch (IOException e) {
            throw new InternalError(e);
        }
        PromotionController controller = loader.getController();

        controller.initRenderer(renderer, ply.piece.player, ply.target);

        Dialog<ButtonType> dlg = GUIUtils.okCancelDialog()
                .owner(owner)
                .title(Messages.getString("promotion.title"))
                .headerText(Messages.getString("promotion.headerText"))
                .content(pane).build();
        Optional<ButtonType> result = dlg.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) return controller.getSelectedPieceType();
        return null;
    }

    @FXML
    private ListView<PieceType> pieceListView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.pieceListView.getItems().addAll(Rules.promotionPieceTypes);
        this.pieceListView.getSelectionModel().select(0);
        Platform.runLater(() -> this.pieceListView.requestFocus());
    }

    private void initRenderer(BoardRenderer renderer, Player player, Coordinate coordinate) {
        this.pieceListView.setCellFactory(new FXPieceTypeCellFactory(renderer, player, coordinate));
    }

    private PieceType getSelectedPieceType() {
        return this.pieceListView.getSelectionModel().getSelectedItem();
    }
}
