
package com.alphachess.view;

import com.alphachess.model.fx.FXLearningFileEntry;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
public class LearningFileEntryOverviewController {
  @FXML
  private TableView<com.alphachess.model.fx.FXLearningFileEntry> fxLearningFileEntryTable;

  @FXML
  private TableColumn<com.alphachess.model.fx.FXLearningFileEntry, String> positionIdentifierColumn;

  @FXML
  private Label positionIdentifierLabel;

  @FXML
  private Label depthLabel;

  @FXML
  private Label scoreLabel;

  @FXML
  private Label moveLabel;

  @FXML
  private Label performanceLabel;

  /**
   *  Reference to the main application.
   */
  private com.alphachess.MainApp mainApp;

  /**
   * Initializes the controller class. This method is automatically called after
   * the fxml file has been loaded.
   */
  @FXML
  private void initialize() {
		// Initialize the FXLearningFileEntry table with the column
		positionIdentifierColumn.setCellValueFactory(cellData -> cellData.getValue().positionIdentifierProperty());
		// Clear person details.
		showFXLearningFileEntryDetails(null);

		// Listen for selection changes and show the person details when changed.
		fxLearningFileEntryTable.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> showFXLearningFileEntryDetails(newValue));

  }

  /**
   * Is called by the main application to give a reference back to itself.
   * 
   * @param mainApp
   */
  public void setMainApp(com.alphachess.MainApp mainApp) {
		this.mainApp = mainApp;

		// Add observable list data to the table
		fxLearningFileEntryTable.setItems(mainApp.getFXLearningFileEntryData());
  }

  /**
   * Fills all text fields to show details about the FXLearningFileEntry. If the
   * specified fxLearningFileEntry is null, all text fields are cleared.
   * 
   * @param person the person or null
   */
  private void showFXLearningFileEntryDetails(com.alphachess.model.fx.FXLearningFileEntry fxLearningFileEntry) {
		if (fxLearningFileEntry != null) {
			// Fill the labels with info from the person object.
			positionIdentifierLabel.setText(fxLearningFileEntry.getPositionIdentifier());
			depthLabel.setText(Integer.toString(fxLearningFileEntry.getDepth()));
			scoreLabel.setText(Integer.toString(fxLearningFileEntry.getScore()));
			moveLabel.setText(fxLearningFileEntry.getMove());
			performanceLabel.setText(Integer.toString(fxLearningFileEntry.getPerformance()));
		} else {
			// FXLearningFileEntry is null, remove all the text.
			positionIdentifierLabel.setText("");
			depthLabel.setText("");
			scoreLabel.setText("");
			moveLabel.setText("");
			performanceLabel.setText("");
		}
  }

  /**
   * Called when the user clicks on the delete button.
   */
  @FXML
  private void handleDeleteLearningFileEntry() {
		int selectedIndex = fxLearningFileEntryTable.getSelectionModel().getSelectedIndex();
		if (selectedIndex >= 0) {
			fxLearningFileEntryTable.getItems().remove(selectedIndex);
		} else {
			// Nothing selected.
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(mainApp.getPrimaryStage());
			alert.setTitle("No Selection");
			alert.setHeaderText("No Learning File Entry Selected");
			alert.setContentText("Please select a learning file entry in the table.");

			alert.showAndWait();
		}
  }

  /**
   * Called when the user clicks the new button. Opens a dialog to edit
   * details for a new person.
   */
  @FXML
  private void handleNewLearningFileEntry() {
	    FXLearningFileEntry tempLearningFileEntry = new FXLearningFileEntry("",0,0,0,0);
	    boolean okClicked = mainApp.showLearningFileEntryEditDialog(tempLearningFileEntry);
	    if (okClicked) {
	        mainApp.getFXLearningFileEntryData().add(tempLearningFileEntry);
	    }
  }

  /**
   * Called when the user clicks the edit button. Opens a dialog to edit
   * details for the selected entry.
   */
  @FXML
  private void handleEditLearningFileEntry() {
	    FXLearningFileEntry selectedLearningFileEntry = fxLearningFileEntryTable.getSelectionModel().getSelectedItem();
	    if (selectedLearningFileEntry != null) {
	        boolean okClicked = mainApp.showLearningFileEntryEditDialog(selectedLearningFileEntry);
	        if (okClicked) {
	            showFXLearningFileEntryDetails(selectedLearningFileEntry);
	        }

	    } else {
	        // Nothing selected.
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("No Selection");
	        alert.setHeaderText("No LearningFileEntry Selected");
	        alert.setContentText("Please select a person in the table.");

	        alert.showAndWait();
	    }
  }

}
