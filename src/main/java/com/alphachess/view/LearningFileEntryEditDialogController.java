
package com.alphachess.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
/**
 * Dialog to edit details of a person.
 * 
 * @author Marco Jakob
 */
public class LearningFileEntryEditDialogController {
  @FXML
  private TextField depthField;

  @FXML
  private TextField scoreField;

  @FXML
  private TextField moveField;


  private Stage dialogStage;

  private com.alphachess.model.fx.FXLearningFileEntry fxLearningFileEntry;

  private boolean okClicked =  false;

  @FXML
  private void initialize() {
        /**
         * Initializes the controller class. This method is automatically called
         * after the fxml file has been loaded.
         */    	
  }

  /**
   * Sets the stage of this dialog.
   * 
   * @param dialogStage
   */
  public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
  }

  /**
   * Sets the person to be edited in the dialog.
   * 
   * @param person
   */
  public void setFXLearningFileEntry(com.alphachess.model.fx.FXLearningFileEntry fxLearningFileEntry) {
        this.fxLearningFileEntry = fxLearningFileEntry;
        depthField.setText(Integer.toString(fxLearningFileEntry.getDepth()));
        scoreField.setText(Integer.toString(fxLearningFileEntry.getScore()));
        moveField.setText(fxLearningFileEntry.getMove());
  }

  /**
   * Returns true if the user clicked OK, false otherwise.
   * 
   * @return
   */
  public boolean isOkClicked() {
        return okClicked;
  }

  /**
   * Called when the user clicks ok.
   */
  @FXML
  private void handleOk() {
        if (isInputValid()) {
            fxLearningFileEntry.setDepth(Integer.parseInt(depthField.getText()));
            fxLearningFileEntry.setScore(Integer.parseInt(scoreField.getText()));
            fxLearningFileEntry.setMove(moveField.getText());
            fxLearningFileEntry.setPerformance(100);
            okClicked = true;
            dialogStage.close();
        }
  }

  /**
   * Called when the user clicks cancel.
   */
  @FXML
  private void handleCancel() {
        dialogStage.close();
  }

  /**
   * Validates the user input in the text fields.
   * 
   * @return true if the input is valid
   */
  private boolean isInputValid() {
        String errorMessage = "";

        if (depthField.getText() == null || depthField.getText().length() == 0) {
            errorMessage += "No valid depth!\n"; 
        } else {
            // try to parse the depth into an int.
            try {
                Integer.parseInt(depthField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "No valid depth (must be an integer)!\n"; 
            }
        }

        if (scoreField.getText() == null || scoreField.getText().length() == 0) {
            errorMessage += "No valid score!\n"; 
        } else {
            // try to parse the score into an int.
            try {
                Integer.parseInt(scoreField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "No valid score (must be an integer)!\n"; 
            }
        }

        if (moveField.getText() == null || moveField.getText().length() == 0) {
            errorMessage += "No valid position identifier!\n"; 
        }

      
        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);
            
            alert.showAndWait();
            
            return false;
        }
  }

}
