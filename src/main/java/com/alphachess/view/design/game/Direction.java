package com.alphachess.view.design.game;

interface Direction {

    int column();

    int row();
}
