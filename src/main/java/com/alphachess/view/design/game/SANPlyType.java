package com.alphachess.view.design.game;

public enum SANPlyType {
    MOVE, KING_SIDE_CASTLING, QUEEN_SIDE_CASTLING
}
