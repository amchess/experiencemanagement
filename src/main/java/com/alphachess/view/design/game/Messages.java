package com.alphachess.view.design.game;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public final class Messages {

    public static final String BUNDLE_NAME = "com/alphachess/view/design/game/messages";

    public static String getString(String key) {
        try {
            return ResourceBundle.getBundle(BUNDLE_NAME).getString(key);
        } catch (MissingResourceException ex) {
            return "!" + key + "!";
        }
    }

    private Messages() {
        // hidden
    }
}
