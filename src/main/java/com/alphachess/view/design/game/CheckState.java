package com.alphachess.view.design.game;

public enum CheckState {

    NONE, CHECK, CHECKMATE;

    public String getDisplayName() {
        return Messages.getString(getClass().getSimpleName() + '.' + name() + ".displayName");
    }
}
