
package com.alphachess.view;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

import com.alphachess.model.fx.FXBoardCanvas;
import com.alphachess.model.fx.FXLearningFileEntry;
import com.alphachess.model.service.UciEngineService;
import com.alphachess.model.service.impl.UciEngineServiceImpl;
import com.alphachess.util.CurrentStatus;
import com.alphachess.util.GenericUtil;
import com.alphachess.view.design.BoardDesign;
import com.alphachess.view.design.game.Board;
import com.alphachess.view.design.game.Coordinate;
import com.alphachess.view.design.game.FEN;
import com.alphachess.view.design.game.Game;

import ictk.boardgame.AmbiguousMoveException;
import ictk.boardgame.IllegalMoveException;
import ictk.boardgame.chess.Bishop;
import ictk.boardgame.chess.ChessBoard;
import ictk.boardgame.chess.ChessGame;
import ictk.boardgame.chess.ChessMove;
import ictk.boardgame.chess.Knight;
import ictk.boardgame.chess.Queen;
import ictk.boardgame.chess.Rook;
import ictk.boardgame.chess.Square;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;

/**
 * The controller for the root layout. The root layout provides the basic
 * application layout containing a menu bar and space where other JavaFX
 * elements can be placed.
 * 
 * @author Marco Jakob
 */
public final class RootLayoutController implements Initializable {
	/**
	 * Reference to the main application
	 */
	private com.alphachess.MainApp mainApp;

	@FXML
	private Pane fxBoardPane;

	private FXBoardCanvas fxCanvas;

	@FXML
	private TextArea logTextArea;

	@FXML
	private TableView<FXLearningFileEntry> experienceTableView;

	@FXML
	private TableColumn<FXLearningFileEntry, String> move;

	@FXML
	private TableColumn<FXLearningFileEntry, Integer> depth;

	@FXML
	private TableColumn<FXLearningFileEntry, Integer> score;
	
	@FXML
	private TableColumn<FXLearningFileEntry, Integer> performance;

	private ObservableList<FXLearningFileEntry> currentFxLearningFileEntryData;

	private CurrentStatus currentStatus = new CurrentStatus();

	private FXLearningFileEntry fxLearningFileEntrySelectedRow;

	ictk.boardgame.chess.io.FEN fen = new ictk.boardgame.chess.io.FEN();

	private File currentExperienceFile;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		this.fxCanvas = new FXBoardCanvas(this);
		initChessBoardGameHistory();
		this.fxBoardPane.getChildren().addAll(this.fxCanvas);
		this.fxCanvas.widthProperty().bind(this.fxBoardPane.widthProperty());
		this.fxCanvas.heightProperty().bind(this.fxBoardPane.heightProperty());
		this.fxCanvas.setCurrentStatus(currentStatus);
		BoardDesign design = BoardDesign.getDesigns().get(0);
		this.fxCanvas.setDesign(design);
		move.setCellValueFactory(new PropertyValueFactory<FXLearningFileEntry, String>("move"));
		depth.setCellValueFactory(new PropertyValueFactory<FXLearningFileEntry, Integer>("depth"));
		score.setCellValueFactory(new PropertyValueFactory<FXLearningFileEntry, Integer>("score"));
		performance.setCellValueFactory(new PropertyValueFactory<FXLearningFileEntry, Integer>("performance"));
		experienceTableView.setPlaceholder(new Label("No experience file entries"));
		logTextArea.autosize();
		experienceTableView.autosize();
		experienceTableView.setRowFactory(etv -> {
			TableRow<FXLearningFileEntry> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				if (!row.isEmpty()) {
					if (event.getClickCount() == 1) {
						fxLearningFileEntrySelectedRow = row.getItem();
					}
					if (event.getClickCount() == 2) {
						FXLearningFileEntry fxLearningFileEntryRow = row.getItem();
						if (fxLearningFileEntryRow != null) {
							String move = fxLearningFileEntryRow.getMove();
							if (move != null) {
								String origSquareStr = move.substring(0, 2);
								String promotionPiece = "";
								selectSquare(origSquareStr, promotionPiece);
								ChessBoard currentChessBoard = currentStatus.getCurrentChessBoard();
								Square origSquare = currentChessBoard.getSquare((byte) (origSquareStr.charAt(0) - 96),
										Byte.parseByte(origSquareStr.substring(1)));
								String destSquareStr = move.substring(2, 4);
								if (move.length() == 5) {
									promotionPiece = move.substring(4, 5);
								}
								selectSquare(destSquareStr, promotionPiece);
								Square destSquare = currentChessBoard.getSquare((byte) (destSquareStr.charAt(0) - 96),
										Byte.parseByte(destSquareStr.substring(1)));
								try {
									ChessMove currentChessMove = new ChessMove(currentChessBoard, origSquare,
											destSquare);
									currentChessMove = GenericUtil.getCastleMove(currentStatus, origSquareStr,
											destSquareStr, currentChessMove);
									currentChessMove = getPromotionMove(move, currentChessMove);
									try {
										currentStatus.getCurrentHistory().add(currentChessMove);
										currentStatus.setCurrentChessBoardFen(fen.boardToString(currentChessBoard));
										setExperienceTableViewItems();
									} catch (AmbiguousMoveException | IndexOutOfBoundsException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								} catch (IllegalMoveException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

						}
					}
				}

			});
			return row;
		});
	    experienceTableView.widthProperty().addListener((obs, oldWidth, newWidth) -> {
	        // Imposta la larghezza delle colonne per riempire lo spazio disponibile
	        double totalWidth = newWidth.doubleValue();
	        move.setPrefWidth(totalWidth * 0.25);   // 25% per "Move"
	        score.setPrefWidth(totalWidth * 0.25);  // 25% per "Value"
	        depth.setPrefWidth(totalWidth * 0.25);  // 25% per "Depth"
	        performance.setPrefWidth(totalWidth * 0.25);  // 25% per "Performance"
	    });
		Platform.runLater(() -> this.fxCanvas.requestFocus());
	}

	private ChessMove getPromotionMove(String move, ChessMove currentChessMove) throws IllegalMoveException {
		if (move.length() == 5) {
			char promoUnit = move.charAt(4);
			int promo = 0;
			switch (promoUnit) {
			case 'q':
				promo = Queen.INDEX;
				break;
			case 'r':
				promo = Rook.INDEX;
				break;
			case 'b':
				promo = Bishop.INDEX;
				break;
			case 'n':
				promo = Knight.INDEX;
				break;
			default:
				break;
			}
			currentChessMove = new ChessMove(currentStatus.getCurrentChessBoard(), ((int) move.charAt(0) - 96),
					(Character.getNumericValue(move.charAt(1))), ((int) move.charAt(2) - 96),
					(Character.getNumericValue(move.charAt(3))), promo);
		}
		return currentChessMove;
	}

	private void initChessBoardGameHistory() {
		currentStatus.setCurrentChessBoardFen(FEN.INITIAL);
		try {
			currentStatus
					.setCurrentChessBoard((ChessBoard) (fen.stringToBoard(GenericUtil.cleanFEN(currentStatus.getCurrentChessBoardFen()))));
			ChessGame currentChessGame = new ChessGame();
			currentChessGame.setBoard(currentStatus.getCurrentChessBoard());
			currentStatus.setCurrentChessGame(currentChessGame);
			currentStatus.setCurrentHistory(currentChessGame.getHistory());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void selectSquare(String square, String promotionPiece) {
		String squareLetter = square.substring(0, 1);
		int columnIndex = getColumnIndexFromOrigSquareLetter(squareLetter);
		int rowIndex = Integer.parseInt(square.substring(1, 2)) - 1;
		Coordinate squareCoordinate = Coordinate.valueOf(columnIndex, rowIndex);
		if (squareCoordinate != null)
			fxCanvas.selectSquare(squareCoordinate, promotionPiece, true);
	}

	private int getColumnIndexFromOrigSquareLetter(String squareLetter) {
		int columnIndex = 0;
		switch (squareLetter) {
		case "a":
			columnIndex = 0;
			break;
		case "b":
			columnIndex = 1;
			break;
		case "c":
			columnIndex = 2;
			break;
		case "d":
			columnIndex = 3;
			break;
		case "e":
			columnIndex = 4;
			break;
		case "f":
			columnIndex = 5;
			break;
		case "g":
			columnIndex = 6;
			break;
		case "h":
			columnIndex = 7;
			break;
		default:
			columnIndex = -1;
			break;
		}
		return columnIndex;
	}

	/**
	 * Is called by the main application to give a reference back to itself.
	 * 
	 * @param mainApp
	 */
	public void setMainApp(com.alphachess.MainApp mainApp) {
		this.mainApp = mainApp;
	}

	@FXML
	private void handleUndo() {
		this.fxCanvas.undo();
		currentStatus.getCurrentHistory().prev();
		currentStatus.setCurrentChessBoard((ChessBoard) currentStatus.getCurrentChessGame().getBoard());
		currentStatus.setCurrentChessBoardFen(fen.boardToString(currentStatus.getCurrentChessBoard()));
		setExperienceTableViewItems();

	}

	@FXML
	private void handleRedo() {
		this.fxCanvas.redo();
		try {
			currentStatus.getCurrentHistory().next();
			currentStatus.setCurrentChessBoard((ChessBoard) currentStatus.getCurrentChessGame().getBoard());
			currentStatus.setCurrentChessBoardFen(fen.boardToString(currentStatus.getCurrentChessBoard()));
			setExperienceTableViewItems();
		} catch (Exception e) {
			GenericUtil.showAlert("No next move", "End of the game",
					"Please enter at least one move.", mainApp.getPrimaryStage());
		}
	}

	/**
	 * Creates an empty xperience bin.
	 */
	@FXML
	private void handleNew() {
		mainApp.getFXLearningFileEntryData().clear();
	}

	/**
	 * Saves the file to the entry file that is currently open. If there is no open
	 * file, the "save as" dialog is shown.
	 */
	@FXML
	private void handleSave() {
		if (currentExperienceFile != null) {
			mainApp.saveLearningFileEntryDataToFile(currentExperienceFile);
			logTextArea.clear();
			logTextArea.appendText("Saved experience file " + currentExperienceFile.getAbsolutePath());
		}
	}

	private File getFile(String description, String... extension) {
		FileChooser fileChooser = new FileChooser();

		// Set extension filter
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(description, extension);
		fileChooser.getExtensionFilters().add(extFilter);
		return fileChooser.showOpenDialog(mainApp.getPrimaryStage());
	}

	/**
	 * Opens a FileChooser to let the user select an experience bin to load.
	 */
	@FXML
	private void handleOpen() {
		currentExperienceFile = getFile("BIN files (*.exp)", "*.exp");

		if (currentExperienceFile != null) {
			mainApp.loadLearningFileEntryDataFromFile(currentExperienceFile);
			currentFxLearningFileEntryData = mainApp.getFXLearningFileEntryData();
			setExperienceFileEntriesSize();
			setExperienceTableViewItems();
		}
	}

	private void setExperienceFileEntriesSize() {
		String logTextAreaExperienceSizeMessage = currentFxLearningFileEntryData.isEmpty() ? "Empty experience file"
				: "Experience file entries: " + currentFxLearningFileEntryData.size();
		logTextArea.clear();
		logTextArea.appendText(logTextAreaExperienceSizeMessage);
	}

	public void setExperienceTableViewItems() {
		if (currentFxLearningFileEntryData != null) {
			ObservableList<FXLearningFileEntry> fxLearningFileEntryDataFromFen = getFXLearningFileEntryDataFromFen(
					currentFxLearningFileEntryData);
			Comparator<FXLearningFileEntry> fXLearningFileEntryComparator = Comparator
					.comparing(FXLearningFileEntry::getPerformance);
			fXLearningFileEntryComparator=fXLearningFileEntryComparator.thenComparing(FXLearningFileEntry::getDepth);
			fXLearningFileEntryComparator=fXLearningFileEntryComparator.thenComparing(FXLearningFileEntry::getScore);
			Collections.sort(fxLearningFileEntryDataFromFen, fXLearningFileEntryComparator);
			FXCollections.reverse(fxLearningFileEntryDataFromFen);
			experienceTableView.setItems(fxLearningFileEntryDataFromFen);
		}
	}

	private ObservableList<com.alphachess.model.fx.FXLearningFileEntry> getFXLearningFileEntryDataFromFen(
			ObservableList<FXLearningFileEntry> fxLearningFileEntryData) {
		ObservableMap<String, String> currentObservableMap = getCurrentObservableMap();
		ObservableList<FXLearningFileEntry> fxLearningFileEntryDataForTable = FXCollections.observableArrayList();
		fxLearningFileEntryData.forEach(fxLearningFileEntry -> {
			String positionIdentifier = fxLearningFileEntry.getPositionIdentifier();
			String currentPositionFen = currentObservableMap.get(positionIdentifier);
			if (currentPositionFen != null && (fxLearningFileEntry.getDepth() != 0)) {
				currentStatus.setCurrentPositionIdentifier(positionIdentifier);
				fxLearningFileEntryDataForTable.add(fxLearningFileEntry);
			}
		});
		return fxLearningFileEntryDataForTable;
	}

	public ObservableMap<String, String> getCurrentObservableMap() {
		List<String> fens = new ArrayList<>();
		fens.add(currentStatus.getCurrentChessBoardFen());
		String uciEnginePath = mainApp.getHashkeyGeneratorFullPath();
		UciEngineService uciEngineService = new UciEngineServiceImpl();
		ObservableMap<String, String> currentObservableMap = uciEngineService.getFensListInHashKeyFenHashMap(fens,
				uciEnginePath, null);
		return currentObservableMap;
	}

	@FXML
	private void openBoard() {
		File file = getFile("FEN files (*.fen)", "*.fen");

		if (file != null) {
			try {
				Board setup = FEN.fromFile(file.toPath());
				String openedFen = GenericUtil.cleanFEN(Files.lines(file.toPath()).limit(1).findFirst()
						.orElseThrow(() -> new IOException("Cannot read empty file")));
				Game game = new Game(setup);
				this.fxCanvas.setGame(game);
				//if (!currentFxLearningFileEntryData.isEmpty()) {
					currentStatus.setCurrentChessBoardFen(openedFen);
					try {
						currentStatus.setCurrentChessBoard(
								(ChessBoard) (fen.stringToBoard(GenericUtil.cleanFEN(currentStatus.getCurrentChessBoardFen()))));
						ChessGame currentChessGame = new ChessGame();
						currentStatus.setCurrentChessGame(currentChessGame);
						currentChessGame.setBoard(currentStatus.getCurrentChessBoard());
						currentStatus.setCurrentHistory(currentChessGame.getHistory());
						ObservableMap<String, String> currentObservableMap = getCurrentObservableMap();
						if (currentObservableMap != null && currentObservableMap.keySet() != null) {
							currentStatus
									.setCurrentPositionIdentifier((String) currentObservableMap.keySet().toArray()[0]);
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					setExperienceTableViewItems();
				//}

			} catch (IOException | IllegalArgumentException e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * Called when the user clicks on the delete button.
	 */
	@FXML
	private void handleDeleteLearningFileEntry() {
		if (fxLearningFileEntrySelectedRow != null) {
			currentFxLearningFileEntryData.remove(fxLearningFileEntrySelectedRow);
			setExperienceTableViewItems();
		} else {
			GenericUtil.showAlert("No Selection", "No Learning File Entry Selected",
					"Please select a learning file entry in the table.", mainApp.getPrimaryStage());
		}
	}

	/**
	 * Called when the user clicks the new button. Opens a dialog to edit details
	 * for a new person.
	 */
	@FXML
	private void handleNewLearningFileEntry() {
		if (currentFxLearningFileEntryData != null) {
			FXLearningFileEntry tempLearningFileEntry = new FXLearningFileEntry(
					currentStatus.getCurrentPositionIdentifier(), 0, 0, 0, 100);
			boolean okClicked = mainApp.showLearningFileEntryEditDialog(tempLearningFileEntry);
			if (okClicked) {
				tempLearningFileEntry.setMoveInt(
						GenericUtil.getIntMove(tempLearningFileEntry.getMove(), currentStatus.getCurrentChessBoard()));
				if (!currentFxLearningFileEntryData.contains(tempLearningFileEntry)) {
					currentFxLearningFileEntryData.add(tempLearningFileEntry);
					setExperienceFileEntriesSize();
					setExperienceTableViewItems();
				}
			}
		} else {
			GenericUtil.showAlert("No experience selectedSelection", "No experience file Selected",
					"Please select an experience file before.", mainApp.getPrimaryStage());
		}
	}

	/**
	 * Called when the user clicks the edit button. Opens a dialog to edit details
	 * for the selected entry.
	 */
	@FXML
	private void handleEditLearningFileEntry() {
		if (fxLearningFileEntrySelectedRow != null) {
			mainApp.showLearningFileEntryEditDialog(fxLearningFileEntrySelectedRow);
			setExperienceTableViewItems();
		} else {
			// Nothing selected.
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(mainApp.getPrimaryStage());
			alert.setTitle("No Selection");
			alert.setHeaderText("No LearningFileEntry Selected");
			alert.setContentText("Please select a learning file entry in the table.");

			alert.showAndWait();
		}
	}

	public TextArea getLogTextArea() {
		return logTextArea;
	}

	public void setLogTextArea(TextArea logTextArea) {
		this.logTextArea = logTextArea;
	}

}
